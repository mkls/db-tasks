--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: weapons; Type: SCHEMA; Schema: -; Owner: mk
--

CREATE SCHEMA weapons;


ALTER SCHEMA weapons OWNER TO mk;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: city; Type: TABLE; Schema: weapons; Owner: mk
--

CREATE TABLE weapons.city (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE weapons.city OWNER TO mk;

--
-- Name: city_id_seq; Type: SEQUENCE; Schema: weapons; Owner: mk
--

CREATE SEQUENCE weapons.city_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE weapons.city_id_seq OWNER TO mk;

--
-- Name: city_id_seq; Type: SEQUENCE OWNED BY; Schema: weapons; Owner: mk
--

ALTER SEQUENCE weapons.city_id_seq OWNED BY weapons.city.id;


--
-- Name: manufacturer; Type: TABLE; Schema: weapons; Owner: mk
--

CREATE TABLE weapons.manufacturer (
    id integer NOT NULL,
    name character varying NOT NULL,
    city integer
);


ALTER TABLE weapons.manufacturer OWNER TO mk;

--
-- Name: manufacturer_id_seq; Type: SEQUENCE; Schema: weapons; Owner: mk
--

CREATE SEQUENCE weapons.manufacturer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE weapons.manufacturer_id_seq OWNER TO mk;

--
-- Name: manufacturer_id_seq; Type: SEQUENCE OWNED BY; Schema: weapons; Owner: mk
--

ALTER SEQUENCE weapons.manufacturer_id_seq OWNED BY weapons.manufacturer.id;


--
-- Name: weapon; Type: TABLE; Schema: weapons; Owner: mk
--

CREATE TABLE weapons.weapon (
    id integer NOT NULL,
    name character varying NOT NULL,
    manufacturer integer NOT NULL
);


ALTER TABLE weapons.weapon OWNER TO mk;

--
-- Name: weapon_id_seq; Type: SEQUENCE; Schema: weapons; Owner: mk
--

CREATE SEQUENCE weapons.weapon_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE weapons.weapon_id_seq OWNER TO mk;

--
-- Name: weapon_id_seq; Type: SEQUENCE OWNED BY; Schema: weapons; Owner: mk
--

ALTER SEQUENCE weapons.weapon_id_seq OWNED BY weapons.weapon.id;


--
-- Name: weapon_manufacturer_seq; Type: SEQUENCE; Schema: weapons; Owner: mk
--

CREATE SEQUENCE weapons.weapon_manufacturer_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE weapons.weapon_manufacturer_seq OWNER TO mk;

--
-- Name: weapon_manufacturer_seq; Type: SEQUENCE OWNED BY; Schema: weapons; Owner: mk
--

ALTER SEQUENCE weapons.weapon_manufacturer_seq OWNED BY weapons.weapon.manufacturer;


--
-- Name: city id; Type: DEFAULT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.city ALTER COLUMN id SET DEFAULT nextval('weapons.city_id_seq'::regclass);


--
-- Name: manufacturer id; Type: DEFAULT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.manufacturer ALTER COLUMN id SET DEFAULT nextval('weapons.manufacturer_id_seq'::regclass);


--
-- Name: weapon id; Type: DEFAULT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.weapon ALTER COLUMN id SET DEFAULT nextval('weapons.weapon_id_seq'::regclass);


--
-- Name: weapon manufacturer; Type: DEFAULT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.weapon ALTER COLUMN manufacturer SET DEFAULT nextval('weapons.weapon_manufacturer_seq'::regclass);


--
-- Data for Name: city; Type: TABLE DATA; Schema: weapons; Owner: mk
--

INSERT INTO weapons.city (id, name) VALUES (1, 'Вятские поляны');
INSERT INTO weapons.city (id, name) VALUES (2, 'Ижевск');
INSERT INTO weapons.city (id, name) VALUES (3, 'Мэдисон');
INSERT INTO weapons.city (id, name) VALUES (4, 'Тула');
INSERT INTO weapons.city (id, name) VALUES (5, 'Норт-Хэйвен');
INSERT INTO weapons.city (id, name) VALUES (6, 'Оберндорф-на-Неккаре');
INSERT INTO weapons.city (id, name) VALUES (7, 'Урбино');


--
-- Data for Name: manufacturer; Type: TABLE DATA; Schema: weapons; Owner: mk
--

INSERT INTO weapons.manufacturer (id, name, city) VALUES (8, 'Heckler & Koch', 6);
INSERT INTO weapons.manufacturer (id, name, city) VALUES (3, 'Ижевский машиностроительный завод', 2);
INSERT INTO weapons.manufacturer (id, name, city) VALUES (2, 'Ижевский механический завод', 2);
INSERT INTO weapons.manufacturer (id, name, city) VALUES (4, 'Тульский оружейный завод', 4);
INSERT INTO weapons.manufacturer (id, name, city) VALUES (7, 'O.F. Mossberg & Sons', 5);
INSERT INTO weapons.manufacturer (id, name, city) VALUES (6, 'Remington Arms', 3);
INSERT INTO weapons.manufacturer (id, name, city) VALUES (5, 'Benelli Armi', 7);
INSERT INTO weapons.manufacturer (id, name, city) VALUES (1, 'Молот оружие', 1);


--
-- Data for Name: weapon; Type: TABLE DATA; Schema: weapons; Owner: mk
--

INSERT INTO weapons.weapon (id, name, manufacturer) VALUES (1, 'HK416', 8);
INSERT INTO weapons.weapon (id, name, manufacturer) VALUES (2, 'Сайга-12', 3);
INSERT INTO weapons.weapon (id, name, manufacturer) VALUES (4, 'Вепрь-12', 1);
INSERT INTO weapons.weapon (id, name, manufacturer) VALUES (6, 'Nova', 5);
INSERT INTO weapons.weapon (id, name, manufacturer) VALUES (8, 'АК-12', 3);
INSERT INTO weapons.weapon (id, name, manufacturer) VALUES (10, 'MP-155', 2);
INSERT INTO weapons.weapon (id, name, manufacturer) VALUES (11, 'Mossberg 500', 7);


--
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: weapons; Owner: mk
--

SELECT pg_catalog.setval('weapons.city_id_seq', 7, true);


--
-- Name: manufacturer_id_seq; Type: SEQUENCE SET; Schema: weapons; Owner: mk
--

SELECT pg_catalog.setval('weapons.manufacturer_id_seq', 8, true);


--
-- Name: weapon_id_seq; Type: SEQUENCE SET; Schema: weapons; Owner: mk
--

SELECT pg_catalog.setval('weapons.weapon_id_seq', 13, true);


--
-- Name: weapon_manufacturer_seq; Type: SEQUENCE SET; Schema: weapons; Owner: mk
--

SELECT pg_catalog.setval('weapons.weapon_manufacturer_seq', 1, false);


--
-- Name: city city_pk; Type: CONSTRAINT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.city
    ADD CONSTRAINT city_pk PRIMARY KEY (id);


--
-- Name: manufacturer manufacturer_pk; Type: CONSTRAINT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.manufacturer
    ADD CONSTRAINT manufacturer_pk PRIMARY KEY (id);


--
-- Name: weapon weapon_pk; Type: CONSTRAINT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.weapon
    ADD CONSTRAINT weapon_pk PRIMARY KEY (id);


--
-- Name: city_id_uindex; Type: INDEX; Schema: weapons; Owner: mk
--

CREATE UNIQUE INDEX city_id_uindex ON weapons.city USING btree (id);


--
-- Name: city_name_uindex; Type: INDEX; Schema: weapons; Owner: mk
--

CREATE UNIQUE INDEX city_name_uindex ON weapons.city USING btree (name);


--
-- Name: manufacturer_id_uindex; Type: INDEX; Schema: weapons; Owner: mk
--

CREATE UNIQUE INDEX manufacturer_id_uindex ON weapons.manufacturer USING btree (id);


--
-- Name: weapon_id_uindex; Type: INDEX; Schema: weapons; Owner: mk
--

CREATE UNIQUE INDEX weapon_id_uindex ON weapons.weapon USING btree (id);


--
-- Name: manufacturer manufacturer_city_id_fk; Type: FK CONSTRAINT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.manufacturer
    ADD CONSTRAINT manufacturer_city_id_fk FOREIGN KEY (city) REFERENCES weapons.city(id);


--
-- Name: weapon weapon_manufacturer_id_fk; Type: FK CONSTRAINT; Schema: weapons; Owner: mk
--

ALTER TABLE ONLY weapons.weapon
    ADD CONSTRAINT weapon_manufacturer_id_fk FOREIGN KEY (manufacturer) REFERENCES weapons.manufacturer(id);


--
-- PostgreSQL database dump complete
--

