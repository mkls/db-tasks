-- Создаём представление
CREATE OR REPLACE VIEW os_interface_view AS
SELECT os_name.name        AS os_name,
       os_version.version,
       release_date,
       interface.name      AS interface_name,
       interface_type.name AS interface_type,
       comment
FROM os_name
         JOIN
     os_version ON os_name.id = os_version.os_id
         JOIN
     os_interface ON os_interface.os_id = os_version.os_id AND os_interface.version = os_version.version
         JOIN
     interface ON interface.id = os_interface.interface_id
         JOIN
     interface_type ON interface.type_id = interface_type.id;

CREATE OR REPLACE FUNCTION view_instead_of_insert()
    RETURNS TRIGGER AS
$$
DECLARE
    _os_id             os.os_name.id%TYPE;
    _interface_id      os.interface.id%TYPE;
    _interface_type_id os.interface_type.id%TYPE;
    _version           os.os_version.version%TYPE;
BEGIN
    SELECT id FROM os.os_name WHERE name = new.os_name INTO _os_id;
    IF _os_id IS NULL THEN
        INSERT INTO os.os_name (name) VALUES (new.os_name);
        SELECT id FROM os.os_name WHERE name = new.os_name INTO _os_id;
    END IF;

    SELECT id FROM os.interface_type WHERE name = new.interface_type INTO _interface_type_id;
    IF _interface_type_id IS NULL THEN
        INSERT INTO os.interface_type (name) VALUES (new.interface_type);
        SELECT id FROM os.interface_type WHERE name = new.interface_type INTO _interface_type_id;
    END IF;

    SELECT id FROM os.interface WHERE name = new.interface_name INTO _interface_id;
    IF _interface_id IS NULL THEN
        INSERT INTO os.interface (name, type_id) VALUES (new.interface_name, _interface_type_id);
        SELECT id FROM os.interface WHERE name = new.interface_name INTO _interface_id;
    END IF;

    SELECT version FROM os.os_version WHERE os_id = _os_id AND version = new.version INTO _version;
    IF _version IS NULL THEN
        INSERT INTO os.os_version (os_id, version, release_date) VALUES (_os_id, new.version, new.release_date);
    ELSE
        INSERT INTO os.os_version (os_id, version, release_date)
        VALUES (_os_id, new.version, new.release_date)
        ON CONFLICT (os_id, version)
            DO UPDATE SET release_date = excluded.release_date;
    END IF;

    INSERT INTO os.os_interface (os_id, version, interface_id, comment)
    VALUES (_os_id, new.version, _interface_id, new.comment)
    ON CONFLICT (os_id, version, interface_id)
        DO UPDATE SET comment = excluded.comment;

    RETURN new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER view_insert_trigger
    INSTEAD OF INSERT
    ON os_interface_view
    FOR EACH ROW
EXECUTE FUNCTION view_instead_of_insert();

CREATE OR REPLACE FUNCTION view_instead_of_update()
    RETURNS TRIGGER AS
$$
DECLARE
    _os_id                 os.os_name.id%TYPE;
    _interface_id          os.interface.id%TYPE;
    _new_interface_id      _interface_id%TYPE;
    _interface_type_id     os.interface_type.id%TYPE;
    _new_interface_type_id _interface_type_id%TYPE;
    _new_os_id             os.os_name.id%TYPE;
    _version               os.os_version.version%TYPE;
BEGIN
    SELECT id FROM os.os_name WHERE name = old.os_name INTO _os_id;
    SELECT id FROM os.os_name WHERE name = new.os_name INTO _new_os_id;

    IF _new_os_id IS NULL THEN
        INSERT INTO os.os_name (name) VALUES (new.os_name);
        SELECT id FROM os.os_name WHERE name = new.os_name INTO _new_os_id;
    END IF;
    --
--     UPDATE os.os_name
--     SET name = new.os_name
--     WHERE id = _os_id;

    -- Костыли-костылики
    INSERT INTO os.os_version (os_id, version, release_date)
    VALUES (_new_os_id, new.version, new.release_date)
    ON CONFLICT (os_id, version)
        DO UPDATE SET release_date = excluded.release_date;

    RAISE NOTICE 'Updating pm with %, old version: %, new version: %', _new_os_id, old.version, new.version;
    UPDATE os.os_pm
    SET os_id      = _new_os_id,
        os_version = new.version
    WHERE os_id = _os_id
      AND os_version = old.version;

    UPDATE os.os_purpose
    SET os_id   = _new_os_id,
        version = new.version
    WHERE os_id = _os_id
      AND version = old.version;

    UPDATE os.os_arch
    SET os_id      = _new_os_id,
        os_version = new.version
    WHERE os_id = _os_id
      AND os_version = old.version;

    SELECT id FROM os.interface_type WHERE name = old.interface_type INTO _interface_type_id;
    SELECT id FROM os.interface_type WHERE name = new.interface_type INTO _new_interface_type_id;

    IF _new_interface_type_id IS NULL THEN
        INSERT INTO os.interface_type (name) VALUES (new.interface_type);
        SELECT id FROM os.interface_type WHERE name = new.interface_type INTO _new_interface_type_id;
--         _new_interface_type_id = _interface_type_id;
    END IF;

    --     UPDATE os.interface_type
--     SET name = new.interface_type
--     WHERE id = _interface_type_id;

    SELECT id FROM os.interface WHERE name = old.interface_name INTO _interface_id;
    SELECT id FROM os.interface WHERE name = new.interface_name INTO _new_interface_id;

    IF _new_interface_id IS NULL THEN
        INSERT INTO os.interface (name, type_id) VALUES (new.interface_name, _new_interface_type_id);
        SELECT id FROM os.interface WHERE name = new.interface_name INTO _new_interface_id;
    END IF;

    --     UPDATE os.interface
--     SET name = new.interface_name
--     WHERE id = _new_interface_id;

--     UPDATE os.os_interface
--     SET os_id        = _new_os_id,
--         version      = new.version
--     WHERE os_id = _os_id AND version = old.version;

    UPDATE os.os_interface
    SET os_id        = _new_os_id,
        version      = new.version,
        interface_id = _new_interface_id,
        comment      = new.comment
    WHERE os_id = _os_id
      AND version = old.version
      AND interface_id = _interface_id;

    --     DELETE FROM os.os_version
--     WHERE os_id = _os_id AND version = old.version;

    RETURN new;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER view_update_trigger
    INSTEAD OF UPDATE
    ON os_interface_view
    FOR EACH ROW
EXECUTE FUNCTION view_instead_of_update();


-- Удаление
CREATE OR REPLACE FUNCTION view_instead_of_delete()
    RETURNS TRIGGER AS
$$
DECLARE
    _os_id        os.os_name.id%TYPE;
    _interface_id os.interface.id%TYPE;
BEGIN
    SELECT id FROM os.os_name WHERE name = old.os_name INTO _os_id;
    SELECT id FROM os.interface WHERE name = old.interface_name INTO _interface_id;

    DELETE FROM os.os_interface
    WHERE os_id = _os_id AND version = old.version AND interface_id = _interface_id;

    RETURN old;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER view_delete_trigger
    INSTEAD OF DELETE
    ON os_interface_view
    FOR EACH ROW
EXECUTE FUNCTION view_instead_of_delete();


INSERT INTO os.os_interface_view (os_name, version, release_date, interface_name, interface_type, comment)
VALUES ('macOS', 'Catalina', null, 'Aqua', 'GUI', 'default interface'),
       ('FreeBSD', '10', null, 'Unix Console', 'CLI', null),
       ('OpenBSD', '6.6', null, 'Unix Console', 'CLI', null);

TABLE os_interface;
TABLE os_name;
TABLE os_version;
TABLE interface;

UPDATE os.os_interface_view
SET os_name = 'Arch'
WHERE os_name LIKE 'Arch %';

UPDATE os.os_interface_view
SET version = 'CURRENT'
WHERE interface_name = 'GNOME 3';

INSERT INTO os.os_interface_view (os_name, version, release_date, interface_name, interface_type, comment)
VALUES ('Arch Linux', 'rolling', null, 'Linux console', 'CLI', null),
       ('Ubuntu', '20.04', '2020-04-23', 'GNOME', 'GUI', 'Patched, unity look and feel'),
       ('Xubuntu', '20.04', '2020-04-23', 'XFCE', 'GUI', '4.14 out of the box');


TABLE os_interface;
TABLE os_name;
TABLE os_version;
TABLE interface;

DELETE FROM os.os_interface_view
WHERE release_date IS NULL;

TABLE os_interface;
TABLE os_name;
TABLE os_version;
TABLE interface;

DROP VIEW IF EXISTS os_interface_view;
DROP TRIGGER view_update_trigger ON os_interface_view;
DROP TRIGGER view_insert_trigger ON os_interface_view;
DROP TRIGGER  view_delete_trigger ON os_interface_view;