import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from psycopg2 import connect
from psycopg2.extras import DictCursor
from psycopg2 import OperationalError
from os import getenv
from datetime import datetime


class TransactionTester:
    def __init__(self):
        builder: Gtk.Builder = Gtk.Builder()
        builder.add_from_file('tester.glade')

        user = getenv('DB_USER')
        password = getenv('DB_PASSWORD')
        host = getenv('DB_HOST')
        port = getenv('DB_PORT')
        db_name = getenv('DB_NAME')

        self.window: Gtk.ApplicationWindow = builder.get_object('transaction_tester')
        self.model: Gtk.TreeModel = builder.get_object('interface_model')
        self.query_chooser: Gtk.ComboBoxText = builder.get_object('app_query_chooser')
        self.save_button: Gtk.Button = builder.get_object('save_button')
        self.save_restore_button: Gtk.Button = builder.get_object('save_restore_button')

        self.connection = connect(
            dbname=db_name,
            user=user,
            password=password,
            host=host,
            port=port
        )

        handlers = {
            'commit_clicked': self.commit_clicked,
            'rollback_clicked': self.rollback_clicked,
            'isolation_level_changed': self.isolation_level_changed,
            'destroy_window': Gtk.main_quit,
            'update_clicked': self.update_clicked,
            'refresh_clicked': self.refresh_clicked,
            'insert_clicked': self.insert_clicked,
            'combo_clicked': self.combo_clicked,
            'save_clicked': self.save,
            'restore_clicked': self.restore_save
        }

        builder.connect_signals(handlers)

        self.__init_model()

    def __del__(self):
        self.connection.close()

    def __init_model(self):
        self.model.clear()

        cur = self.connection.cursor(cursor_factory=DictCursor)
        cur.execute('''SELECT id, name, type_id FROM os.interface''')
        for i, n, t in cur.fetchall():
            self.model.append((i, n, str(t)))

    def update_clicked(self, _):
        cur = self.connection.cursor(cursor_factory=DictCursor)
        cur.execute(
            '''UPDATE os.interface SET name = %s WHERE name ILIKE %s''',
            ("I just got updated at {}".format(str(datetime.utcnow())),
             r"%GNOME%")
        )
        self.__init_model()

    def insert_clicked(self, _):
        cur = self.connection.cursor(cursor_factory=DictCursor)
        cur.execute(
            '''INSERT INTO os.interface (name, type_id) VALUES ('I JUST GOT INSERTED', %s)''',
            (1,)
        )
        self.__init_model()

    def combo_clicked(self, _):
        model = self.query_chooser.get_model()
        active = self.query_chooser.get_active_iter()

        cur = self.connection.cursor()

        query_text = 'INSERT INTO os.interface (name, type_id) ' \
                     'SELECT COUNT(*)::varchar, %s FROM os.interface ' \
                     'WHERE type_id = %s '

        if model[active][0] == 'FIRST':
            cur.execute(query_text, (1, 2))
        else:
            cur.execute(query_text, (2, 1))
        self.__init_model()

    def save(self, _):
        cur = self.connection.cursor()
        cur.execute('SAVEPOINT s1')
        self.save_restore_button.props.sensitive = True
        self.save_button.props.sensitive = False

    def restore_save(self, _):
        cur = self.connection.cursor()
        cur.execute('ROLLBACK TO SAVEPOINT s1')
        cur.execute('RELEASE SAVEPOINT s1')
        self.save_button.props.sensitive = True
        self.save_restore_button.props.sensitive = False

    def commit_clicked(self, _):
        try:
            self.connection.commit()
        except OperationalError:
            message_dialog = Gtk.MessageDialog(parent=self.window)
            message_dialog.set_modal(True)
            message_dialog.set_transient_for(self.window)
            upper, lower = message_dialog.get_message_area()
            upper.set_text("SERIALIZATION ERROR")
            lower.set_text("Rolling back")
            message_dialog.show_all()
            self.connection.rollback()
        self.__init_model()

    def rollback_clicked(self, _):
        self.connection.rollback()
        self.__init_model()

    def isolation_level_changed(self, combo: Gtk.ComboBoxText):
        self.connection.rollback()

        model = combo.get_model()
        active = combo.get_active_iter()

        self.connection.isolation_level = model[active][0]

    def refresh_clicked(self, _):
        self.__init_model()

    def run(self):
        self.window.show_all()
        Gtk.main()
