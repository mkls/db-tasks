from tester import TransactionTester as App


def main():
    app = App()
    app.run()


if __name__ == '__main__':
    main()
