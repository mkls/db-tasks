import gi

gi.require_version('Gtk', '3.0')

from ui_login import AuthWindow
from ui_table_view import Viewer
from gi.repository import Gtk
from util import UserStatus


class Application:
    def __init__(self, session_obj, db_conn_str=None):
        self.session = session_obj
        self.auth_window = AuthWindow(db_conn_str)
        self.table_viewer = Viewer(session_obj, UserStatus.GUEST)

    def start(self):
        self.auth_window.show()
        Gtk.main()

        status = self.auth_window.status
        logged_in = self.auth_window.logged_in
        self.table_viewer.set_visibility(status)

        if status is not None and logged_in:
            self.table_viewer.show_all()
            Gtk.main()

    def load_data_from_list(self, target, items):
        if self.table_viewer is None:
            return

        self.table_viewer.load_data_from_list(target, items)

    def load_tuples(self, target, items):
        self.table_viewer.load_tuples(target, items)
