import gi
import psycopg2

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk
from os import getenv
from model import OsName, OsVersion, PackageManager, PackageManagerType, OsPm


class QueryView:
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('query_runner.glade')

        self.window = self.builder.get_object('query_window')
        self.run_query_button = self.builder.get_object('run_query_button')
        self.query_view = self.builder.get_object('query_view')

        self.os_radio = self.builder.get_object('os_radio')
        self.os_combobox = self.builder.get_object('os_combobox')
        self.query_os_combo = self.builder.get_object('query_os_combo')

        self.pm_entry = self.builder.get_object('pm_name_entry')

        self.os_store = self.builder.get_object('os_store')
        self.pm_store = self.builder.get_object('pm_store')

        # 0 - PM, 1 - OS
        self.toggled_stat = 0

        # Name
        # Type
        # Format
        # Delta updates
        self.pm_renderers = (
            Gtk.CellRendererText(),
            Gtk.CellRendererText(),
            Gtk.CellRendererText(),
            # Gtk.CellRendererToggle()
            Gtk.CellRendererText()
        )

        # Name
        # Version
        # Package manager
        self.os_renderers = (
            Gtk.CellRendererText(),
            Gtk.CellRendererText()
        )

        self.last_model = None

        self.pm_columns = (
            Gtk.TreeViewColumn('Name', self.pm_renderers[0], text=0),
            Gtk.TreeViewColumn('Type', self.pm_renderers[1], text=1),
            Gtk.TreeViewColumn('Format', self.pm_renderers[2], text=2),
            # Gtk.TreeViewColumn('Delta', self.pm_renderers[3], activatable=3)
            Gtk.TreeViewColumn('Count', self.pm_renderers[2], text=3)
        )

        self.os_columns = (
            Gtk.TreeViewColumn('Name', self.os_renderers[0], text=0),
            Gtk.TreeViewColumn('Version', self.os_renderers[1], text=1)
        )

        handlers = {
            'on_win_destroy': self.on_destroy,
            'on_group_change': self.on_group_change,
            'get_top_five': self.get_top_five_clicked,
            'get_latest_releases': self.get_latest_clicked
        }

        self.builder.connect_signals(handlers)

    def on_group_change(self, radio):
        if radio is self.os_radio:
            self.toggled_stat = 1
        else:
            self.toggled_stat = 0

    def get_latest_clicked(self, *args):
        if self.last_model is not None:
            self.last_model.clear()
        target_cols = self.os_columns
        target_model = self.os_store
        self.last_model = target_model

        query = '''SELECT name, version
                   FROM os.os_name
                        LEFT JOIN
                        os.os_version ON os_name.id = os_version.os_id
                   WHERE date_part('year', release_date) = 2020;'''

        for col in self.query_view.get_columns():
            self.query_view.remove_column(col)

        target_model.clear()
        for col in target_cols:
            self.query_view.append_column(col)
        self.query_view.set_model(target_model)

        db_user = getenv('DB_USER')
        db_name1 = getenv('DB_NAME2')
        db_pass = getenv('DB_PASSWORD')

        db_str = 'host=localhost user=' + db_user + ' dbname=' + db_name1

        with psycopg2.connect(db_str) as conn:
            with conn.cursor() as cur:
                cur.execute(query)

                for res in cur.fetchall():
                    target_model.append(res)
            cur.close()
        conn.close()

    def get_top_five_clicked(self, *args):
        if self.last_model is not None:
            self.last_model.clear()
        target_cols = self.pm_columns
        target_model = self.pm_store
        self.last_model = target_model

        for col in self.query_view.get_columns():
            self.query_view.remove_column(col)

        query = '''SELECT package_manager.name, pkg_format, package_manager_type.name, COUNT(*) as cnt
                    FROM os.package_manager JOIN os.os_pm ON package_manager.id = os_pm.pm_id
                    JOIN os.package_manager_type ON package_manager.type_id = package_manager_type.id
                    GROUP BY package_manager.name, pkg_format, package_manager_type.name
                    ORDER BY cnt DESC
                    LIMIT 5;'''

        target_model.clear()
        for col in target_cols:
            self.query_view.append_column(col)
        self.query_view.set_model(target_model)

        db_user = getenv('DB_USER')
        db_name1 = getenv('DB_NAME2')
        db_pass = getenv('DB_PASSWORD')

        db_str = 'host=localhost user=' + db_user + ' dbname=' + db_name1

        with psycopg2.connect(db_str) as conn:
            with conn.cursor() as cur:
                cur.execute(query)

                for res in cur.fetchall():
                    target_model.append(res)

    def on_destroy(self, *args):
        self.window.hide()

    def set_model(self, os_names):
        for t in os_names:
            self.query_os_combo.append((str(t), t))
        self.os_combobox.set_model(self.query_os_combo)
        self.os_combobox.set_id_column(0)
        renderer_text = Gtk.CellRendererText()
        self.os_combobox.pack_start(renderer_text, True)
        self.os_combobox.add_attribute(renderer_text, "text", 0)
        # renderer = Gtk.CellRendererText()
        # self.os_combobox.add_attribute(renderer, 'text', 0)

    def show_all(self):
        self.window.show_all()
