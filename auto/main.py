from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from os import getenv
from app import Application

import model


def main():
    db_user = getenv('DB_USER')
    db_name1 = getenv('DB_NAME1')
    db_name2 = getenv('DB_NAME2')
    db_pass = getenv('DB_PASSWORD')

    db_str = 'host=localhost user=' + db_user + ' dbname=' + db_name1

    engine = create_engine(
        'postgresql://{}:{}@localhost/{}'.format(db_user, db_pass, db_name2),
        echo=True
    )

    session = sessionmaker(bind=engine)()
    package_managers = session.query(model.PackageManager). \
        order_by(model.PackageManager.name)
    pm_types = session.query(model.PackageManagerType). \
        order_by(model.PackageManagerType.name)
    os_names = session.query(model.OsName). \
        order_by(model.OsName.name)
    os_versions = session.query(model.OsVersion). \
        order_by(model.OsVersion.os_id)
    os_pms = session.query(model.OsPm). \
        order_by(model.OsPm.os_id)

    vers_c = session.query(model.OsName, model.OsVersion). \
        filter(model.OsName.id == model.OsVersion.os_id). \
        order_by(model.OsName.name)

    # For some reason GtkCellRendererCombo creates its own column for
    # model internally and requires that the text column is *exactly*
    # text, no casting allowed. Pity.
    pm_types_combo = []
    os_names_combo = []
    pm_names_combo = []
    os_versions_combo = []
    for t in pm_types:
        pm_types_combo.append((str(t), t))
    pm_types_combo.append(('<empty>', None))

    for t in os_names:
        os_names_combo.append((str(t), t))

    for t in package_managers:
        pm_names_combo.append((str(t), t))

    for _, t in vers_c:
        os_versions_combo.append((str(t), t))

    app = Application(session, db_str)
    app.load_data_from_list(app.table_viewer.pm_store, package_managers)
    app.load_data_from_list(app.table_viewer.pm_type_store, pm_types)
    app.load_data_from_list(app.table_viewer.os_name_store, os_names)
    app.load_data_from_list(app.table_viewer.os_version_store, os_versions)
    app.load_data_from_list(app.table_viewer.os_pm_store, os_pms)
    app.load_tuples(app.table_viewer.os_combo_store, os_names_combo)
    app.load_tuples(app.table_viewer.pm_combo_store, pm_names_combo)
    app.load_tuples(app.table_viewer.pm_type_combo, pm_types_combo)
    app.load_tuples(app.table_viewer.os_versions_combo_store, os_versions_combo)
    app.start()


if __name__ == '__main__':
    main()
