import gi
import re

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

from model import PackageManager, PackageManagerType, find_dup, OsName, OsVersion, OsPm
from util import UserStatus
from crud import CrudWindow
from enum import Enum
from query_window import QueryView


class Pages(Enum):
    PACKAGE_MANAGER = 0
    PACKAGE_MANAGER_TYPE = 1
    OS_NAME = 2
    VERSION = 3
    OS_PM = 4


class Viewer:
    def __init__(self, session, user_status):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('table_view.glade')

        self.user_status = user_status

        self.session = session

        # Main table view window
        self.window = self.builder.get_object('app_window')

        self.dirty_iters = {}

        # Store models
        self.pm_store = self.builder.get_object('pm_store')
        self.pm_type_store = self.builder.get_object('pm_type_store')
        self.pm_type_combo = self.builder.get_object('pm_type_combo')
        self.os_name_store = self.builder.get_object('os_names')
        self.os_version_store = self.builder.get_object('os_versions')
        self.os_pm_store = self.builder.get_object('os_pm')

        self.os_combo_store = self.builder.get_object('os_combo_store')
        self.pm_combo_store = self.builder.get_object('pm_combo_store')
        self.os_versions_combo_store = self.builder.get_object('os_versions_combo_store')

        # Headerbar buttons
        self.add_row_button = self.builder.get_object('add_row_button')
        self.remove_row_button = self.builder.get_object('remove_row_button')

        # Notebook
        self.notebook = self.builder.get_object('notebook')

        # Notebook operation buttons
        self.commit_button = self.builder.get_object('commit_button')
        self.restore_button = self.builder.get_object('restore_button')

        # Show users crud button
        self.show_users_button = self.builder.get_object('user_view_button')

        # Infobar
        self.info_bar = self.builder.get_object('info_bar')
        self.info_bar_label = self.builder.get_object('info_bar_label')

        # Columns
        ## Package Manager
        self.pm_name_column = self.builder.get_object('pm_name_column')
        self.pm_type_column = self.builder.get_object('pm_type_column')
        self.pm_format_column = self.builder.get_object('pm_format_column')
        self.pm_delta_column = self.builder.get_object('pm_delta_column')

        ## Package Manager Type
        self.type_column = self.builder.get_object('type_column')
        self.description_column = self.builder.get_object('description_column')

        ## OS Name
        self.os_name_column = self.builder.get_object('os_name_column')

        ## OS Version
        self.os_name_v_column = self.builder.get_object('os_name_v_column')
        self.os_version_column = self.builder.get_object('os_version_column')

        ## OS PM
        self.os_v_column = self.builder.get_object('os_v_column')
        self.pm_v_column = self.builder.get_object('pm_v_column')

        # Cell renderers
        ## Package Manager
        self.pm_name_renderer = self.builder.get_object('pm_name_renderer')
        self.type_name_renderer = self.builder.get_object('type_name_renderer')
        self.pkg_format_renderer = self.builder.get_object('pkg_format_renderer')
        self.pm_delta_renderer = self.builder.get_object('pm_delta_renderer')

        ## Package Manager Type
        self.pm_type_renderer = self.builder.get_object('pm_type_renderer')
        self.description_renderer = self.builder.get_object('description_renderer')

        ## OS Name
        self.os_name_renderer = self.builder.get_object('os_name_renderer')

        ## OS Version
        self.os_name_v_renderer = self.builder.get_object('os_name_v_combo')
        self.version_renderer = self.builder.get_object('version_renderer')

        ## OS PM
        self.os_v_renderer = self.builder.get_object('os_v_renderer')
        self.os_pm_renderer = self.builder.get_object('os_pm_renderer')

        # Tree views
        self.pm_view = self.builder.get_object('pm_view')
        self.pm_type_view = self.builder.get_object('pm_type_view')
        self.os_name_view = self.builder.get_object('os_name_view')
        self.version_view = self.builder.get_object('version_view')
        self.os_pm_view = self.builder.get_object('os_pm_view')

        # Tree selections
        self.pm_selection = self.builder.get_object('pm_selection')
        self.pm_type_selection = self.builder.get_object('pm_type_selection')
        self.os_name_selection = self.builder.get_object('os_name_selection')
        self.version_selection = self.builder.get_object('version_selection')
        self.os_pm_selection = self.builder.get_object('os_pm_selection')

        # Connect to signals, set renderer functions
        self.set_renderers()
        self.connect_handlers()

    def set_renderers(self):
        self.pm_name_column.set_cell_data_func(
            self.pm_name_renderer,
            self.render_text,
            'pm_name'
        )

        self.pm_type_column.set_cell_data_func(
            self.type_name_renderer,
            self.render_text,
            'type_name'
        )

        self.pm_format_column.set_cell_data_func(
            self.pkg_format_renderer,
            self.render_text,
            'pm_pkg_format'
        )

        self.type_column.set_cell_data_func(
            self.pm_type_renderer,
            self.render_text,
            'name'
        )

        self.description_column.set_cell_data_func(
            self.description_renderer,
            self.render_text,
            'description'
        )

        self.pm_delta_column.set_cell_data_func(
            self.pm_delta_renderer,
            self.render_checkbox,
            'pm_delta'
        )

        self.os_name_column.set_cell_data_func(
            self.os_name_renderer,
            self.render_text,
            'name'
        )

        self.os_name_v_column.set_cell_data_func(
            self.os_name_v_renderer,
            self.render_text,
            'os_name'
        )

        self.os_version_column.set_cell_data_func(
            self.version_renderer,
            self.render_text,
            'version'
        )

        self.os_v_column.set_cell_data_func(
            self.os_v_renderer,
            self.render_text,
            'os_v'
        )

        self.pm_v_column.set_cell_data_func(
            self.os_pm_renderer,
            self.render_text,
            'pm_name'
        )

    def connect_handlers(self):
        self.pm_name_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'pm_name'
        )

        self.type_name_renderer.connect(
            'changed',
            self.combo_changed,
            1,
            'pm_type_id',
            'id',
            'pm_type',
            self.pm_store
        )

        self.pkg_format_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'pm_pkg_format'
        )

        self.pm_delta_renderer.connect(
            'toggled',
            self.checkbox_toggled
        )

        self.pm_type_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'name'
        )

        self.description_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'description'
        )

        self.os_name_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'name'
        )

        self.os_name_v_renderer.connect(
            'changed',
            self.os_combo_changed,
            self.os_version_store
        )

        self.version_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'version'
        )

        self.os_v_renderer.connect(
            'changed',
            self.pm_combo_changed,
            'os'
        )

        self.os_pm_renderer.connect(
            'changed',
            self.pm_combo_changed,
            'pm'
        )

        self.commit_button.connect(
            'clicked',
            self.commit_clicked
        )

        self.restore_button.connect(
            'clicked',
            self.restore_clicked
        )

        handlers = {
            'on_destroy': Gtk.main_quit,
            'on_text_entry_edited': self.on_text_entry_updated,
            'on_insert_clicked': self.on_insert_clicked,
            'on_selection_changed': self.selection_changed,
            'on_delete_row_clicked': self.delete_clicked,
            'editing_sync': self.editing_sync,
            'editing-canceled': self.editing_canceled,
            'on_user_view_button_clicked': self.on_user_view_button_clicked,
            'on_run_query_button_clicked': self.on_run_query_button_clicked
        }

        self.builder.connect_signals(handlers)

    def on_user_view_button_clicked(self, *args):
        # Users window
        crud_window = CrudWindow()
        crud_window.window.set_modal(True)
        crud_window.window.set_transient_for(self.window)

        crud_window.show()

    def append_to_storage(self, target, item):
        target.append(item)

    def load_tuples(self, target, tuples):
        for t in tuples:
            target.append(t)

    def load_data_from_list(self, target, list):
        for item in list:
            target.append((item,))

    def load_from_db(self):
        pass

    def show_all(self):
        self.window.show_all()

    def render_checkbox(self, column, cell, model, model_iter, prop):
        obj = model[model_iter][0]
        if obj.get_value('pm_delta') is None:
            obj.set_value('pm_delta', False)

        cell.set_property('active', obj.get_value(prop))
        self.update_cell_color(cell, model, model_iter)

    def render_text(self, column, cell, model, model_iter, prop):
        proxy_item = model[model_iter][0]
        if proxy_item is None:
            text = None
        else:
            text = proxy_item.get_value(prop)
            if isinstance(text, OsVersion):
                text = str(text)
        if text is None:
            text = '<empty>'

        cell.set_property('text', text)
        self.update_cell_color(cell, model, model_iter)

    def get_model_str_repr(self, model):
        if model is self.pm_store:
            return 'pm'
        if model is self.pm_type_store:
            return 'pm_type'
        if model is self.os_name_store:
            return 'os_name'
        if model is self.os_version_store:
            return 'os_version'
        if model is self.os_pm_store:
            return 'os_pm'
        return None

    def on_text_entry_updated(self, cell, path, new_text, prop):
        trimmed_text = re.sub(' +', ' ', new_text)
        trimmed_text = trimmed_text.strip()

        current_model = self.model_from_page(self.notebook.get_current_page())

        prop_type = type(current_model[path][0].get_value(prop))
        if prop_type is type(None):
            current_model[path][0].set_value(prop, trimmed_text)
        else:
            current_model[path][0].set_value(prop, prop_type(trimmed_text))

        m = self.get_model_str_repr(current_model)
        if (m, path) not in self.dirty_iters:
            self.dirty_iters[(m, path)] = 'm'

        self.sync_buttons()

    def pm_combo_changed(self, combo, str_path, new_iter, *args):
        obj = combo.props.model[new_iter][1]
        model = self.os_pm_store
        a = args[0]

        if a == 'pm':
            if obj is None:
                model[str_path][0].set_value('pm_id', None)
            else:
                model[str_path][0].set_value('pm_id', obj.get_value('pm_id'))
            model[str_path][0].set_value('pm', obj)
        else:
            if obj is None:
                model[str_path][0].set_value('os_id', None)
                model[str_path][0].set_value('os_version', None)
            else:
                model[str_path][0].set_value('os_id', obj.get_value('os_id'))
                model[str_path][0].set_value('os_version', obj.get_value('version'))
            model[str_path][0].set_value('os_v', obj)
        self.sync_buttons()

    def os_combo_changed(self, combo, str_path, new_iter, *args):
        obj = combo.props.model[new_iter][1]
        model = args[0]

        if obj is None:
            model[str_path][0].set_value('os_id', None)
        else:
            model[str_path][0].set_value('os_id', obj.get_value('os_id'))

        if model is self.os_version_store:
            model[str_path][0].set_value('name', obj)

        m = self.get_model_str_repr(model)
        if (m, str_path) not in self.dirty_iters:
            self.dirty_iters[(m, str_path)] = 'm'
        self.sync_buttons()

    def combo_changed(self, combo, str_path, new_iter, *args):
        """args contains object column num, property name and updated model"""
        obj = combo.props.model[new_iter][args[0]]
        prop1 = args[1]
        prop2 = args[2]
        prop3 = args[3]
        model = args[4]

        if obj is None:
            model[str_path][0].set_value(prop1, None)
        else:
            model[str_path][0].set_value(prop1, obj.get_value(prop2))
        model[str_path][0].set_value(prop3, obj)

        m = self.get_model_str_repr(model)
        if (m, str_path) not in self.dirty_iters:
            self.dirty_iters[(m, str_path)] = 'm'
        self.sync_buttons()

    def set_visibility(self, status):
        self.user_status = status
        if self.user_status == UserStatus.ADMIN:
            self.show_users_button.props.visible = True
            self.show_users_button.props.sensitive = True
        elif self.user_status == UserStatus.GUEST:
            self.pm_name_renderer.props.editable = False
            self.type_name_renderer.props.editable = False
            self.pkg_format_renderer.props.editable = False
            self.pm_delta_renderer.props.activatable = False
            self.pm_type_renderer.props.editable = False
            self.description_renderer.props.editable = False
            self.os_name_renderer.props.editable = False
            self.os_name_v_renderer.props.editable = False
            self.version_renderer.props.editable = False
            self.os_v_renderer.props.editable = False
            self.os_pm_renderer.props.editable = False

            self.add_row_button.props.sensitive = False
            self.remove_row_button.props.sensitive = False

    def checkbox_toggled(self, cell, path):
        obj = self.pm_store[path][0]
        if obj.get_value('pm_delta') is None:
            val = False
        else:
            val = obj.get_value('pm_delta') ^ True
        obj.set_value('pm_delta', val)

        m = 'pm'
        if (m, path) not in self.dirty_iters:
            self.dirty_iters[(m, path)] = 'm'
        self.sync_buttons()

    def sync_buttons(self, modification=False):
        dirty = len(self.session.dirty) > 0
        new = len(self.session.new) > 0
        deleted = len(self.session.deleted) > 0

        if (dirty or new or deleted) and not modification:
            self.restore_button.set_sensitive(True)
            self.commit_button.set_sensitive(True)
        else:
            self.restore_button.set_sensitive(False)
            self.commit_button.set_sensitive(False)

    def set_info_bar(self, msg: str, show: bool):
        self.info_bar_label.set_text(msg)
        self.info_bar.set_revealed(show)

    def selection_changed(self, selection):
        if selection.get_selected() is None:
            self.sync_buttons(modification=True)
        else:
            self.sync_buttons()

    def type_from_page(self, page):
        if page == Pages.PACKAGE_MANAGER.value:
            return PackageManager
        if page == Pages.PACKAGE_MANAGER_TYPE.value:
            return PackageManagerType
        if page == Pages.OS_NAME.value:
            return OsName
        if page == Pages.VERSION.value:
            return OsVersion
        if page == Pages.OS_PM.value:
            return OsPm
        return None

    def on_insert_clicked(self, *args):
        current_model = self.model_from_page(self.notebook.get_current_page())
        current_view = self.view_from_page(self.notebook.get_current_page())
        current_type = self.type_from_page(self.notebook.get_current_page())

        selection = self.selection_from_page(self.notebook.get_current_page())
        # selection = current_view.get_selection()
        new_entry = current_type()
        self.session.add(new_entry)
        new_iter = current_model.append((new_entry,))
        path_str = current_model.get_string_from_iter(new_iter)
        m = self.get_model_str_repr(current_model)
        self.dirty_iters[(m, path_str)] = 'i'
        selection.select_iter(new_iter)

        self.sync_buttons()

    def view_from_page(self, page_num):
        if page_num == Pages.PACKAGE_MANAGER:
            return self.pm_view
        if page_num == Pages.PACKAGE_MANAGER_TYPE:
            return self.pm_type_view
        if page_num == Pages.OS_NAME:
            return self.os_name_view
        if page_num == Pages.VERSION:
            return self.version_view
        if page_num == Pages.OS_PM:
            return self.os_pm_view

    def model_from_str_repr(self, repr: str):
        if repr == 'pm':
            return self.pm_store
        if repr == 'pm_type':
            return self.pm_type_store
        if repr == 'os_name':
            return self.os_name_store
        if repr == 'os_version':
            return self.os_version_store
        if repr == 'os_pm':
            return self.os_pm_store
        return None

    def commit_clicked(self, *args):
        self.set_info_bar('', False)
        iters = []
        for (m, p), s in self.dirty_iters.items():
            if s is None:
                continue
            model = self.model_from_str_repr(m)
            if s == 'd':
                iters.append(
                    (model, model.get_iter_from_string(p))
                )
            else:
                if model is self.pm_store:
                    props = ('name', 'type_id', 'delta_support', 'pkg_format')
                    selection = self.pm_selection
                elif model is self.pm_type_store:
                    props = ('name', 'description')
                    selection = self.pm_type_selection
                elif model is self.os_name_store:
                    props = ('name',)
                    selection = self.os_name_selection
                elif model is self.os_version_store:
                    props = ('os_id', 'version')
                    selection = self.version_selection
                else:
                    props = ('os_id', 'os_version', 'pm_id')
                    selection = self.os_pm_selection

                p_iter = model.get_iter_from_string(p)
                item = model[p][0]
                if model is not self.os_pm_store and getattr(item, 'name') is None:
                    self.set_info_bar('Name cannot be empty', True)
                    return
                elif model is self.os_pm_store and (getattr(item, 'os_id') is None or
                                                    getattr(item, 'os_version') is None or
                                                    getattr(item, 'pm_id') is None):
                    self.set_info_bar('You need to fill all columns in this table', True)
                    return
                if find_dup(model, item, *props):
                    selection.select_iter(p_iter)
                    self.set_info_bar('Duplicates are not allowed here', True)
                    return

        for m, i in iters:
            m.remove(i)

        package_managers = self.session.query(PackageManager). \
            order_by(PackageManager.name)
        pm_types = self.session.query(PackageManagerType). \
            order_by(PackageManagerType.name)
        os_names = self.session.query(OsName). \
            order_by(OsName.name)
        os_versions = self.session.query(OsName, OsVersion). \
            filter(OsName.id == OsVersion.os_id). \
            order_by(OsName.name)

        pm_types_combo = []
        os_names_combo = []
        pm_names_combo = []
        os_versions_combo = []
        for t in pm_types:
            pm_types_combo.append((str(t), t))
        pm_types_combo.append(('<empty>', None))

        for t in os_names:
            os_names_combo.append((str(t), t))

        for t in package_managers:
            pm_names_combo.append((str(t), t))

        for _, t in os_versions:
            os_versions_combo.append((str(t), t))

        self.pm_type_combo.clear()
        self.os_combo_store.clear()
        self.os_versions_combo_store.clear()
        self.pm_combo_store.clear()

        self.load_tuples(self.pm_type_combo, pm_types_combo)
        self.load_tuples(self.os_combo_store, os_names_combo)
        self.load_tuples(self.pm_combo_store, pm_names_combo)
        self.load_tuples(self.os_versions_combo_store, os_versions_combo)

        iters.clear()

        self.session.commit()
        self.sync_buttons()
        self.clear_dirty_dict()

    def restore_clicked(self, *args):
        self.set_info_bar('', False)
        iters = []
        for (m, p), s in self.dirty_iters.items():
            if s is None:
                continue
            if s == 'i':
                model = self.model_from_str_repr(m)
                p_iter = model.get_iter_from_string(p)
                iters.append((model, p_iter))

        for model, iter in iters:
            model.remove(iter)

        self.session.rollback()
        self.sync_buttons()
        self.clear_dirty_dict()

    def clear_dirty_dict(self):
        self.dirty_iters.clear()

    def editing_canceled(self):
        self.sync_buttons(modification=False)

    def editing_sync(self, cell, editable, path):
        self.sync_buttons(modification=True)

    def update_cell_color(self, cell, model, cell_iter):
        m = self.get_model_str_repr(model)

        path_str = model.get_string_from_iter(cell_iter)
        key = (m, path_str)
        if key in self.dirty_iters:
            if self.dirty_iters[key] == 'd':
                cell.props.cell_background_rgba = Gdk.RGBA(0.8, 0, 0, 0.4)
            elif self.dirty_iters[key] == 'm':
                cell.props.cell_background_rgba = Gdk.RGBA(1, 1, 0.5, 0.7)
            elif self.dirty_iters[key] == 'i':
                cell.props.cell_background_rgba = Gdk.RGBA(0, 0.4, 0.1, 0.4)
        else:
            cell.props.cell_background_rgba = None

    def model_from_page(self, page_num):
        if page_num == Pages.PACKAGE_MANAGER.value:
            return self.pm_store
        if page_num == Pages.PACKAGE_MANAGER_TYPE.value:
            return self.pm_type_store
        if page_num == Pages.OS_NAME.value:
            return self.os_name_store
        if page_num == Pages.VERSION.value:
            return self.os_version_store
        if page_num == Pages.OS_PM.value:
            return self.os_pm_store

    def selection_from_page(self, page_num):
        if page_num == Pages.PACKAGE_MANAGER.value:
            return self.pm_selection
        if page_num == Pages.PACKAGE_MANAGER_TYPE.value:
            return self.pm_type_selection
        if page_num == Pages.OS_NAME.value:
            return self.os_name_selection
        if page_num == Pages.VERSION.value:
            return self.version_selection
        if page_num == Pages.OS_PM.value:
            return self.os_pm_selection

    def mark_to_delete(self, val, target_src, field):
        m = self.get_model_str_repr(target_src)
        for row in target_src:
            if val == getattr(row[0], field):
                self.dirty_iters[(m, str(row.path))] = 'd'

    def mark_version(self, id, version, target_src):
        m = self.get_model_str_repr(target_src)
        for row in target_src:
            if row[0].os_id == id and row[0].version == version:
                self.dirty_iters[(m, str(row.path))] = 'd'

    def delete_clicked(self, *args):
        m = self.get_model_str_repr(self.model_from_page(self.notebook.get_current_page()))
        current_selection = self.selection_from_page(self.notebook.get_current_page())

        model, selected = current_selection.get_selected()
        path_str = model.get_string_from_iter(selected)
        if (m, path_str) in self.dirty_iters and \
                self.dirty_iters[(m, path_str)] == 'i':
            self.dirty_iters[(m, path_str)] = None
        else:
            self.dirty_iters[(m, path_str)] = 'd'

            if model is self.os_name_store:
                self.mark_to_delete(model[path_str][0].id, self.os_version_store, 'os_id')
                self.mark_to_delete(model[path_str][0].id, self.os_pm_store, 'os_id')
            elif model is self.os_version_store:
                self.mark_version(model[path_str][0].id,
                                  model[path_str][0].version,
                                  self.os_pm_store)
            elif model is self.pm_store:
                self.mark_to_delete(model[path_str][0].id, self.os_pm_store, 'pm_id')

        obj = model[selected][0]

        if obj not in self.session.new:
            self.session.delete(obj)
        else:
            model.remove(selected)
            self.session.expunge(obj)

        self.set_info_bar('', False)
        self.sync_buttons()

    def on_run_query_button_clicked(self, *args):
        # os_names = self.session.query(OsName). \
        #     order_by(OsName.id)
        query_view = QueryView()
        query_view.window.set_modal(True)
        query_view.window.set_transient_for(self.window)
        # query_view.set_model(os_names)
        query_view.show_all()
