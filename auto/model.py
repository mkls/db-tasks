from sqlalchemy import Column, Integer, Boolean, String, ForeignKey, ForeignKeyConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class OsVersion(Base):
    __tablename__ = 'os_version'
    __table_args__ = {'schema': 'os'}

    os_id = Column(
        Integer,
        ForeignKey(
            'os.os_name.id',
            onupdate='CASCADE',
            ondelete='CASCADE'
        ),
        primary_key=True,
    )
    version = Column(
        String,
        nullable=False,
        primary_key=True
    )

    name = relationship(
        'OsName',
        uselist=False,
        back_populates='versions',
        cascade='delete'
    )

    fields = {
        'os_id': 'os_id',
        'version': 'version',
        'name': 'name'
    }

    os_fields = {
        'os_name': 'name'
    }

    def __str__(self):
        return '{}: {}'.format(self.name, self.version)

    def get_value(self, field: str):
        if field in self.os_fields:
            if self.name is None:
                return None
            return self.name.get_value(self.os_fields[field])
        return getattr(self, self.fields[field])

    def set_value(self, field: str, value) -> None:
        if field in self.os_fields:
            self.name.set_value(self.os_fields[field], value)
        else:
            setattr(self, self.fields[field], value)


class OsPm(Base):
    __tablename__ = 'os_pm'
    __table_args__ = {'schema': 'os'}

    os_id = Column(Integer, primary_key=True)
    os_version = Column(String, primary_key=True)
    pm_id = Column(
        Integer,
        ForeignKey(
            'os.package_manager.id',
            ondelete='CASCADE',
            onupdate='CASCADE'
        ),
        primary_key=True
    )

    ForeignKeyConstraint(
        (os_id, os_version),
        (OsVersion.os_id, OsVersion.version),
        onupdate='CASCADE',
        ondelete='CASCADE'
    )

    fields = {
        'os_id': 'os_id',
        'version': 'os_version',
        'os_version': 'os_version',
        'os_v': 'os_v',
        'pm_id': 'pm_id',
        'pm': 'pm'
    }

    os_fields = {
        'os_name': 'os_name'
    }

    pm_fields = {
        'pm_name': 'pm_name'
    }

    os_v = relationship(
        'OsVersion',
        uselist=False
    )

    pm = relationship(
        'PackageManager',
        uselist=False,
        back_populates='os_versions'
    )

    def get_value(self, field: str):
        if field in self.os_fields:
            if self.os_v is None:
                return None
            return self.os_v.get_value(self.os_fields[field])
        elif field in self.pm_fields:
            if self.pm is None:
                return None
            return self.pm.get_value(self.pm_fields[field])
        return getattr(self, self.fields[field])

    def set_value(self, field: str, value) -> None:
        if field in self.os_fields:
            self.os_v.set_value(self.os_fields[field], value)
        else:
            setattr(self, self.fields[field], value)


class OsName(Base):
    __tablename__ = 'os_name'
    __table_args__ = {'schema': 'os'}

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)

    versions = relationship('OsVersion', back_populates='name', cascade='delete')

    fields = {
        'os_id': 'id',
        'name': 'name'
    }

    def __str__(self):
        return '{}'.format(self.name)

    def get_value(self, field: str):
        return getattr(self, self.fields[field])

    def set_value(self, field: str, value) -> None:
        setattr(self, self.fields[field], value)


class PackageManagerType(Base):
    __tablename__ = 'package_manager_type'
    __table_args__ = {'schema': 'os'}

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)

    fields = {
        'id': 'id',
        'name': 'name',
        'description': 'description'
    }

    package_managers = relationship(
        'PackageManager',
        back_populates='type'
    )

    def get_value(self, field: str):
        return getattr(self, self.fields[field])

    def set_value(self, field: str, value) -> None:
        setattr(self, self.fields[field], value)

    def __str__(self):
        return '{}'.format(self.name)

    def __repr__(self):
        return '<PackageManagerType(id={}, name={}, description={})>'. \
            format(self.id, self.name, self.description)


class PackageManager(Base):
    __tablename__ = 'package_manager'
    __table_args__ = {'schema': 'os'}

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, default='')
    type_id = Column(
        Integer,
        ForeignKey(
            'os.package_manager_type.id',
            ondelete='SET NULL',
            onupdate='CASCADE'
        )
    )
    pkg_format = Column(String)
    delta_support = Column(Boolean, nullable=False, default=True)

    fields = {
        'name': 'name',
        'pm_id': 'id',
        'pm_name': 'name',
        'pm_type_id': 'type_id',
        'pm_pkg_format': 'pkg_format',
        'pm_delta': 'delta_support',
        'pm_type': 'type'
    }

    type_fields = {
        'type_name': 'name',
        'type_description': 'description'
    }

    type = relationship(
        'PackageManagerType',
        uselist=False,
        back_populates='package_managers'
    )

    os_versions = relationship(
        'OsPm',
        back_populates='pm'
    )

    def get_value(self, field: str):
        if field in self.type_fields:
            if self.type is None:
                return None
            return self.type.get_value(self.type_fields[field])
        return getattr(self, self.fields[field])

    def set_value(self, field: str, value) -> None:
        if field in self.type_fields:
            self.type.set_value(self.type_fields[field], value)
        else:
            setattr(self, self.fields[field], value)

    def __str__(self):
        return '{}'.format(self.name)

    def __repr__(self):
        return '<PackageManager(id={}, name={}, type_id={}, format={})>' \
               '\ttype: {}'. \
            format(self.id, self.name, self.type_id, self.pkg_format, self.type)


def compare_items(item1, item2, *props):
    for i in props:
        if getattr(item1, i) != getattr(item2, i):
            return False
    return True


def find_dup(source, item, *props):
    for i in source:
        if i[0] is not item and compare_items(i[0], item, *props):
            return True
    return False
