from enum import Enum


class UserStatus(Enum):
    GUEST = 0
    USER = 1
    ADMIN = 2
