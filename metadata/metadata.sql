CREATE TABLE spec
(
    id          INTEGER PRIMARY KEY,
    table_name  VARCHAR,
    column_name VARCHAR,
    max_val     INTEGER
);

INSERT INTO spec(id, table_name, column_name, max_val)
VALUES (1, 'spec', 'id', 1);

SELECT table_name, column_name, data_type
FROM information_schema.columns
WHERE table_name = 'sample_text';

CREATE OR REPLACE FUNCTION spec_on_upsert()
    RETURNS TRIGGER
AS
$$
DECLARE
    tmp spec.max_val%TYPE;
BEGIN
    EXECUTE format('SELECT MAX(%I) FROM new', quote_ident(tg_argv[0])) INTO tmp;
    UPDATE spec
    SET max_val = tmp
    WHERE table_name = tg_table_name
      AND column_name = tg_argv[0]
      AND tmp > max_val;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION find_new_id(n_table CHARACTER VARYING, n_column CHARACTER VARYING) RETURNS INTEGER
    LANGUAGE plpgsql
AS
$$
DECLARE
    __tmp_counter     spec.max_val%TYPE;
    __trigger_counter spec.max_val%TYPE;
BEGIN
    ASSERT EXISTS(SELECT *
                  FROM information_schema.columns
                  WHERE table_name = n_table), 'No such table';
    ASSERT EXISTS(SELECT *
                  FROM information_schema.columns
                  WHERE table_name = n_table
                    AND column_name = n_column),'No such column';
    ASSERT (SELECT data_type
            FROM information_schema.columns
            WHERE table_name = n_table
              AND column_name = n_column) = 'integer', 'Wrong data type';

    SELECT COUNT(DISTINCT trigger_name) + 1
    INTO __trigger_counter
    FROM information_schema.triggers
    WHERE event_object_table = n_table;


    IF EXISTS(SELECT * FROM spec WHERE TABLE_NAME = n_table AND COLUMN_NAME = n_column) THEN
        UPDATE spec SET max_val = max_val + 1 WHERE TABLE_NAME = n_table AND COLUMN_NAME = n_column;
    ELSE
        EXECUTE FORMAT('SELECT COALESCE(MAX(%I), 0) FROM %I', n_column, n_table) INTO __tmp_counter;
        LOOP
            IF EXISTS(SELECT *
                  FROM information_schema.triggers
                  WHERE trigger_name = n_table || '_' || n_column || '_' || __trigger_counter
                     OR trigger_name = n_table || '_' || n_column || '_' || __trigger_counter + 1
            ) THEN
                __trigger_counter = __trigger_counter + 1;
            ELSE
                EXIT;
            END IF;
        END LOOP;

        EXECUTE FORMAT(
                'CREATE TRIGGER %I '
                    'AFTER INSERT '
                    'ON %I '
                    'REFERENCING NEW TABLE AS new '
                    'FOR EACH STATEMENT '
                    'EXECUTE FUNCTION spec_on_upsert(%I)',
                n_table || '_' || n_column || '_' || __trigger_counter, n_table, n_column);
        EXECUTE FORMAT(
                'CREATE TRIGGER %I '
                    'AFTER UPDATE '
                    'ON %I '
                    'REFERENCING NEW TABLE AS new '
                    'FOR EACH STATEMENT '
                    'EXECUTE FUNCTION spec_on_upsert(%I)',
                n_table || '_' || n_column || '_' || __trigger_counter + 1, n_table, n_column);

        INSERT INTO spec (id, TABLE_NAME, COLUMN_NAME, max_val)
        VALUES (find_new_id('spec', 'id'), n_table, n_column, __tmp_counter + 1);
    END IF;

    RETURN (SELECT max_val FROM spec WHERE TABLE_NAME = n_table AND COLUMN_NAME = n_column);
END;
$$;

CREATE TABLE test
(
    id   INT,
    name VARCHAR
);

INSERT INTO test(id, name)
VALUES (find_new_id('test', 'id'), 'Oh Hello There');

CREATE TABLE sample_text
(
    id                 INTEGER,
    name               VARCHAR,
    really_cool_number INTEGER
);

INSERT INTO sample_text(id, name, really_cool_number)
VALUES (find_new_id('sample_text', 'id'), 'Super cool table', 42);

INSERT INTO sample_text(id, name, really_cool_number)
VALUES (100500, 'ff', find_new_id('sample_text', 'really_cool_number'));

INSERT INTO sample_text(id, name, really_cool_number)
VALUES (find_new_id('sample_text', 'id'),
        E'I\'m still super cool table',
        find_new_id('sample_text', 'really_cool_number'));

INSERT INTO test(id, name)
VALUES (25, 'triggering insert trigger'),
       (55, 'triggering insert trigger'),
       (32, 'triggering insert trigger');

INSERT INTO sample_text(id, name, really_cool_number)
VALUES (47, 'abracadabra', 99),
       (32, 'вжух', 58),
       (45, 'отчислен', 64);

CREATE TABLE "test me pls"
(
    id   INTEGER,
    name VARCHAR
);

CREATE TRIGGER "test me pls_id_5"
    AFTER INSERT
    ON "test me pls"
    REFERENCING NEW TABLE AS new
    FOR EACH ROW
EXECUTE FUNCTION spec_on_upsert('id');

CREATE TRIGGER aaa
    AFTER INSERT
    ON "test me pls"
    REFERENCING NEW TABLE AS new
    FOR EACH ROW
EXECUTE FUNCTION spec_on_upsert('id');

CREATE TRIGGER bbb
    AFTER INSERT
    ON "test me pls"
    REFERENCING NEW TABLE AS new
    FOR EACH ROW
EXECUTE FUNCTION spec_on_upsert('id');

CREATE TRIGGER ccc
    AFTER INSERT
    ON "test me pls"
    REFERENCING NEW TABLE AS new
    FOR EACH ROW
EXECUTE FUNCTION spec_on_upsert('id');

INSERT INTO "test me pls"(id, name)
VALUES (find_new_id('test me pls', 'id'), 'тестим пробелы');

DO
$$
    DECLARE
        message TEXT;
    BEGIN
        SELECT find_new_id('super_cool_table', 'id');
    EXCEPTION
        WHEN assert_failure THEN
                GET STACKED DIAGNOSTICS MESSAGE = MESSAGE_TEXT;
                RAISE EXCEPTION 'Whoops, error occured: %', MESSAGE;
    END;
$$;
END;

DO
$$
    DECLARE
        message TEXT;
    BEGIN
        SELECT find_new_id('sample_text', 'name');
    EXCEPTION
        WHEN assert_failure THEN
                GET STACKED DIAGNOSTICS MESSAGE = MESSAGE_TEXT;
                RAISE EXCEPTION 'Whoops, error occured: %', MESSAGE;
    END;
$$;
END;

DO
$$
    DECLARE
        message TEXT;
    BEGIN
        SELECT find_new_id('sample_text', 'super_id');
    EXCEPTION
        WHEN assert_failure THEN
                GET STACKED DIAGNOSTICS MESSAGE = MESSAGE_TEXT;
                RAISE EXCEPTION 'Whoops, error occured: %', MESSAGE;
    END;
$$;
END;

SELECT find_new_id('test', 'name');
SELECT find_new_id('super_cool_table', 'id');

DROP TABLE test;
DROP TABLE sample_text;
DROP TABLE "test me pls"
DROP TABLE spec;
DROP FUNCTION find_new_id(n_table CHARACTER VARYING, n_column CHARACTER VARYING);

