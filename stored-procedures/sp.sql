-- ХП
CREATE TABLE spec
(
    id          INTEGER PRIMARY KEY,
    table_name  VARCHAR,
    column_name VARCHAR,
    max_val     INTEGER
);

INSERT INTO spec(id, table_name, column_name, max_val)
VALUES (1, 'spec', 'id', 1);

CREATE OR REPLACE FUNCTION find_new_id(n_table CHARACTER VARYING, n_column CHARACTER VARYING) RETURNS INTEGER
    LANGUAGE plpgsql
AS
$$
DECLARE
    __tmp_counter spec.max_val%TYPE;
BEGIN
    IF EXISTS(SELECT * FROM spec WHERE table_name = n_table AND column_name = n_column) THEN
        UPDATE spec SET max_val = max_val + 1 WHERE table_name = n_table AND column_name = n_column;
    ELSE
        EXECUTE format('SELECT MAX(%I) FROM %I', quote_ident(n_column), quote_ident(n_table)) INTO __tmp_counter;

        IF __tmp_counter IS NULL THEN
            __tmp_counter = 0;
        END IF;

        INSERT INTO spec (id, table_name, column_name, max_val)
        VALUES (find_new_id('spec', 'id'), n_table, n_column, __tmp_counter + 1);
    END IF;

    RETURN (SELECT max_val FROM spec WHERE TABLE_NAME = n_table AND COLUMN_NAME = n_column);
END;
$$;

SELECT find_new_id('spec', 'id');
SELECT * FROM spec;
SELECT find_new_id('spec', 'id');
SELECT * FROM spec;

CREATE TABLE test (
    id INTEGER
);
INSERT INTO test (id) VALUES (10);

SELECT find_new_id('test', 'id');
SELECT * FROM spec;
SELECT find_new_id('test', 'id');
SELECT * FROM spec;

CREATE TABLE test2 (
    num_value1 INTEGER,
    num_value2 INTEGER
);

SELECT find_new_id('test2', 'num_value1');
SELECT * FROM spec;
SELECT find_new_id('test2', 'num_value1');
SELECT * FROM spec;

INSERT INTO test2 (num_value1, num_value2) VALUES (2, 13);
SELECT find_new_id('test2', 'num_value2');
SELECT * FROM spec;

SELECT find_new_id('test2', 'num_value1');
SELECT find_new_id('test2', 'num_value1');
SELECT find_new_id('test2', 'num_value1');
SELECT find_new_id('test2', 'num_value1');
SELECT find_new_id('test2', 'num_value1');
SELECT * FROM spec;

DROP FUNCTION find_new_id;
DROP TABLE test;
DROP TABLE test2;
DROP TABLE spec;
