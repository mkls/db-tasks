from psycopg2 import connect, sql
from psycopg2.extras import DictCursor


class DbProxy:
    __result_query = sql.SQL('')
    __select = sql.SQL('SELECT ')
    __from = sql.SQL('FROM ')
    __where = sql.SQL('WHERE ')
    __order = sql.SQL('ORDER')

    def __init__(self, user, db_name, password, port, host):
        self.connection = connect(
            dbname=db_name,
            user=user,
            password=password,
            port=port,
            host=host
        )

    def __del__(self):
        self.connection.close()

    @staticmethod
    def __add_arguments(query_obj, convert_type, *args):
        query_obj.join('{args}').format(
            args=sql.SQL(', ').join([convert_type(i) for i in args])
        )

    def execute_select(self, query):
        with self.connection.cursor(cursor_factory=DictCursor) as cur:
            cur.execute(query)
            return cur.fetchall()

    def obtain_args(self, query, *args):
        lowered_query = query.lower()
        if lowered_query == 'select':
            self.__add_arguments(self.__select, sql.Identifier, args)
        elif lowered_query == 'from':
            self.__add_arguments(self.__from, sql.Identifier, args)
        elif lowered_query == 'where':
            self.__add_arguments(self.__where, sql.Identifier, args)
        elif lowered_query == 'order':
            self.__add_arguments(self.__order, args)
        else:
            raise NameError

    @staticmethod
    def get_final_query(selection, tables, links, filters, order):
        selection = sql.SQL('SELECT\n\t{identifiers}\n').format(
            identifiers=sql.SQL(',\n\t').join(map(sql.SQL, selection))
        )
        tables = sql.SQL('FROM\n\t{tables}\n').format(
            tables=sql.SQL(',\n\t').join(map(sql.Identifier, tables))
        )
        if len(links) > 0:
            links = sql.SQL('WHERE\n\t{links}').format(
                links=sql.SQL(' AND\n\t').join(map(sql.SQL, links)),
            )
        else:
            links = sql.SQL('')
        if len(filters) > 0:
            links = links + sql.SQL(' AND (\n\t')
            links = links + sql.SQL('\n\t').join(filters)
            links = links + sql.SQL(')\n')
        else:
            links = links + sql.SQL('\n\t')
        if order is not None and len(order) > 0:
            links += sql.SQL('\n')
            order = sql.SQL('ORDER BY\n\t{sort_order}').format(
                sort_order=sql.SQL(',\n\t').join(order)
            )
        else:
            order = sql.SQL('')

        return selection + tables + links + order

    @staticmethod
    def generate_filter_statement(table_name: str,
                                  col_name: str,
                                  op: str,
                                  val,
                                  sep):
        s = sql.SQL('{t}.{c} {op} {v} {sep}').format(
            t=sql.Identifier(table_name),
            c=sql.Identifier(col_name),
            op=sql.SQL(op),
            v=sql.Literal(val),
            sep=sql.SQL(sep)
        )
        return s

    @staticmethod
    def gen_order_statement(table_name: str,
                            col_name: str,
                            order: str):
        s = sql.SQL('{table}.{column} {order}').format(
            table=sql.Identifier(table_name),
            column=sql.Identifier(col_name),
            order=sql.SQL(order)
        )
        return s

    def load_data(self, sources, items):
        query = sql.SQL('SELECT {identifiers} '
                        'FROM {sources}').format(
            identifiers=sql.SQL(', ').join(map(sql.Identifier, items)),
            sources=sql.SQL(', ').join(map(sql.Identifier, sources))
        )
        with self.connection.cursor(cursor_factory=DictCursor) as cur:
            cur.execute(query)
            return cur.fetchall()
