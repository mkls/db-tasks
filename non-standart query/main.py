from ui import NonstandardQueryViewer as App
from proxy import DbProxy
from os import getenv


def main():
    user = getenv('DB_USER')
    password = getenv('DB_PASSWORD')
    dbname = getenv('DB_NAME')
    port = getenv('DB_PORT')

    app = App(DbProxy(user, dbname, password, port, 'localhost'))
    app.start()


if __name__ == '__main__':
    main()
