import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from proxy import DbProxy
from datetime import date
from decimal import Decimal, DecimalException, InvalidOperation


class NonstandardQueryViewer:
    def __init__(self, proxy: DbProxy):
        # Init builder object
        builder: Gtk.Builder = Gtk.Builder()
        builder.add_from_file('ui.glade')

        # Relation dicts
        self.direct_links = {}
        self.non_direct_links = {}

        # Db proxy
        self.proxy: DbProxy = proxy

        # Get objects from builder
        self.main_window: Gtk.ApplicationWindow = \
            builder.get_object('main_window')
        self.preview_window: Gtk.Window = \
            builder.get_object('query_preview_window')

        self.add_to_selected_button: Gtk.Button = \
            builder.get_object('add_to_selected_button')
        self.add_all_to_selected_button: Gtk.Button = \
            builder.get_object('add_all_to_selected_button')
        self.remove_from_selected_button: Gtk.Button = \
            builder.get_object('remove_from_selected_button')
        self.remove_all_from_selected_button: Gtk.Button = \
            builder.get_object('remove_all_from_selected_button')
        self.add_to_sorted_button: Gtk.Button = \
            builder.get_object('add_to_sorted_button')
        self.add_all_to_sorted_button: Gtk.Button = \
            builder.get_object('add_all_to_sorted_button')
        self.remove_from_sorted_button: Gtk.Button = \
            builder.get_object('remove_from_sorted_button')
        self.remove_all_from_sorted_button: Gtk.Button = \
            builder.get_object('remove_all_from_sorted_button')
        self.add_filter_button: Gtk.Button = \
            builder.get_object('add_filter_button')
        self.remove_filter_button: Gtk.Button = \
            builder.get_object('remove_filter_button')
        self.preview_button: Gtk.Button = \
            builder.get_object('preview_query_button')
        self.run_button: Gtk.Button = \
            builder.get_object('run_query_button')

        self.column_filter_combo: Gtk.ComboBox = \
            builder.get_object('column_filter_combo')
        self.restriction_combo: Gtk.ComboBox = \
            builder.get_object('restriction_combo')
        self.filter_entry: Gtk.Entry = \
            builder.get_object('filter_entry')
        self.filter_combo: Gtk.ComboBox = \
            builder.get_object('filter_combo')
        self.link_combo: Gtk.ComboBoxText = \
            builder.get_object('join_combo')

        self.cell_renderer_sort: Gtk.CellRendererToggle = \
            builder.get_object('cell_renderer_sort')

        self.notebook: Gtk.Notebook = \
            builder.get_object('notebook')

        self.preview_text: Gtk.TextBuffer = \
            builder.get_object('query_text_buffer')

        self.select_columns_view: Gtk.TreeView = \
            builder.get_object('select_columns_view')
        self.selected_columns_view: Gtk.TreeView = \
            builder.get_object('selected_columns_view')
        self.filter_view: Gtk.TreeView = \
            builder.get_object('filter_view')
        self.order_view: Gtk.TreeView = \
            builder.get_object('order_view')
        self.order_selected_view: Gtk.TreeView = \
            builder.get_object('order_selected_view')
        self.result_view: Gtk.TreeView = \
            builder.get_object('result_view')

        self.columns: Gtk.ListStore = \
            builder.get_object('columns')
        self.columns_copy: Gtk.ListStore = \
            builder.get_object('columns_copy')
        self.string_filters: Gtk.ListStore = \
            builder.get_object('string_filters')
        self.numeric_filters: Gtk.ListStore = \
            builder.get_object('integer_filters')
        self.boolean_filters: Gtk.ListStore = \
            builder.get_object('boolean_filters')
        self.selected_columns: Gtk.ListStore = \
            builder.get_object('selected_columns')
        self.order_selected: Gtk.ListStore = \
            builder.get_object('order_selected')
        self.filter_store: Gtk.ListStore = \
            builder.get_object('filter_store')
        self.to_select_to_order: Gtk.ListStore = \
            builder.get_object('order_to_select')

        # Load data from database
        self.load_columns()

        # Connect signals with handlers
        self.connect_signals()

    def connect_signals(self):
        self.main_window.connect('destroy', Gtk.main_quit)
        self.preview_window.connect('delete-event', self.on_preview_closed)
        self.preview_button.connect('clicked', self.on_preview_clicked)
        self.add_to_selected_button.connect(
            'clicked',
            self.on_move_item_clicked,
            self.select_columns_view,
            self.selected_columns_view,
            2,
            self.to_select_to_order
        )
        self.remove_from_selected_button.connect(
            'clicked',
            self.on_move_item_clicked,
            self.selected_columns_view,
            self.select_columns_view,
            1,
            self.to_select_to_order,
            self.order_selected
        )
        self.column_filter_combo.connect(
            'changed',
            self.filter_target_changed
        )
        self.add_filter_button.connect(
            'clicked',
            self.add_filter_clicked
        )
        self.remove_filter_button.connect(
            'clicked',
            self.remove_filter_clicked
        )
        self.add_to_sorted_button.connect(
            'clicked',
            self.move_to_sorted
        )
        self.remove_from_sorted_button.connect(
            'clicked',
            self.move_from_sorted
        )
        self.cell_renderer_sort.connect(
            'toggled',
            self.on_toggled
        )
        self.run_button.connect(
            'clicked',
            self.on_run_query_clicked
        )

    def start(self):
        self.main_window.show_all()
        Gtk.main()

    def on_toggled(self, _, path: str):
        self.order_selected[path][4] ^= True

    def move_to_sorted(self, _):
        selection: Gtk.TreeSelection = self.order_view.get_selection()
        model, i = selection.get_selected()
        item = model[i]
        self.order_selected.append(tuple(item) + (False,))
        model.remove(i)

    def move_from_sorted(self, *args):
        selection: Gtk.TreeSelection = self.order_selected_view.get_selection()
        model, i = selection.get_selected()
        item = tuple(model[i])
        self.to_select_to_order.append(item[:-1])
        model.remove(i)

    def add_filter_clicked(self, *args):
        column_model = self.column_filter_combo.get_model()
        active_iter = self.column_filter_combo.get_active_iter()
        column = tuple(column_model[active_iter])
        res_type = self.type_from_str(column[2])

        message_dialog = Gtk.MessageDialog(parent=self.main_window)
        message_dialog.set_modal(True)
        message_dialog.set_transient_for(self.main_window)
        upper, lower = message_dialog.get_message_area()

        if self.filter_entry.get_text() == "":
            upper.set_text('Ошибка: пустое значение в поле фильтра'.format(res_type))
            message_dialog.show_all()
            return

        restriction = tuple(self.restriction_combo.get_model()[
                                self.restriction_combo.get_active_iter()
                            ])
        text = (self.filter_entry.get_text(),)
        try:
            res_type(text[0])
        except (ValueError, InvalidOperation):
            upper.set_text('Ошибка: недопустимое значение для фильтра типа {}'.format(res_type))
            message_dialog.show_all()
            return
        link = (tuple(self.link_combo.get_model()[
                          self.link_combo.get_active_iter()
                      ])[1].upper(),)
        print(column + restriction + text + link)
        self.filter_store.append(column + restriction + text + link)

    def remove_filter_clicked(self, *args):
        selection = self.filter_view.get_selection()
        model, i = selection.get_selected()

        if i is not None:
            model.remove(i)

    def load_columns(self):
        # for i in self.proxy.load_data(
        #         ['helpers.fields'],
        #         ['transl_fn', 'field_name', 'field_type', 'table_name']):
        #     self.columns.append(i)
        #     self.columns_copy.append(i)

        for i in self.proxy.execute_select(
                'SELECT transl_fn, field_name, field_type, table_name '
                'FROM helpers.fields'):
            self.columns.append(i)
            self.columns_copy.append(i)

        for s1, s2, s3 in self.proxy.execute_select(
                'SELECT table1, table2, relations '
                'FROM helpers.rel_table '
                'WHERE relations IS NOT NULL'):
            self.direct_links[(s1, s2)] = s3

        for s1, s2, s3 in self.proxy.execute_select(
                'SELECT table1, table2, via '
                'FROM helpers.rel_table '
                'WHERE relations IS NULL'):
            self.non_direct_links[(s1, s2)] = s3
            self.non_direct_links[(s2, s1)] = s3

    def on_move_item_clicked(self, *args):
        """Args is should contain to values: source1 (from) and source2 (to)
           also if additional sources are provided, they are gonna be refreshed
           with values from first additional arg (full reload) """
        source1: Gtk.TreeView = args[1]
        source2: Gtk.TreeView = args[2]

        selection1: Gtk.TreeSelection = source1.get_selection()

        m, i = selection1.get_selected()
        if i is not None:
            model1: Gtk.ListStore = m
            model2: Gtk.ListStore = source2.get_model()

            item = model1.get(i, *range(model1.get_n_columns()))
            model2.append(item)
            m.remove(i)
            self.order_selected.clear()

            if len(args) > 3:
                r = args[4]
                r.clear()
                for j in args[args[3]].get_model():
                    r.append(tuple(j))
                if len(args) == 6:
                    args[5].clear()

    def filter_target_changed(self, combo: Gtk.ComboBox):
        model = combo.get_model()
        i_item = combo.get_active_iter()
        item = model[i_item]
        # str_name, real_name, data_type, table_name
        if item[2] == 'integer' \
                or item[2] == 'double precision' \
                or item[2] == 'float' \
                or item[2] == 'datetime':
            self.restriction_combo.set_model(self.numeric_filters)
        elif item[2] == 'character varying':
            self.restriction_combo.set_model(self.string_filters)
        elif item[2] == 'boolean':
            self.restriction_combo.set_model(self.boolean_filters)
        else:
            raise NotImplementedError

    @staticmethod
    def type_from_str(type_str: str):
        if type_str == 'integer':
            return int
        if type_str == 'character varying':
            return str
        if type_str == 'date':
            return date
        if type_str == 'boolean':
            return bool
        if type_str == 'double precision':
            return Decimal
        return NotImplementedError

    def get_filter_clauses(self):
        # str_name, real_name, data_type, table_name, restriction, val, link
        tuples = [tuple(i) for i in self.filter_store]
        statements = []
        for t in tuples:
            end = t[6]
            # if t[6] == 'END' and t[6] is not tuples[-1]:
            #     message_dialog = Gtk.MessageDialog(parent=self.main_window)
            #     message_dialog.set_modal(True)
            #     message_dialog.set_transient_for(self.main_window)
            #     upper, lower = message_dialog.get_message_area()
            #     upper.set_text('Пустая связка должна быть только в конце!')
            #     message_dialog.show_all()
            #     return None

            if t[6] == 'END':
                end = ''
            res_type = self.type_from_str(t[2])
            res_val = None
            try:
                res_val = res_type(t[5])
            except ValueError:
                print('invalid value for type {}'.format(res_type))
                return
            statements.append(
                self.proxy.generate_filter_statement(
                    t[3],
                    t[1],
                    t[4],
                    res_val,
                    end
                )
            )
        for s in statements:
            print(s.as_string(self.proxy.connection))
        return statements

    def get_order_statements(self):
        tuples = [tuple(i) for i in self.order_selected]
        statements = []
        for t in tuples:
            if t[4]:
                d = 'ASC'
            else:
                d = 'DESC'
            statements.append(
                self.proxy.gen_order_statement(t[3], t[1], d)
            )
        return statements

    def compose_query(self):
        column_names = set()
        target_tables = set()
        link_clauses = set()
        filter_clauses = self.get_filter_clauses()
        order_statements = self.get_order_statements()

        tuples = [tuple(i) for i in self.selected_columns]

        # (str_name, real_name, data_type, table_name)
        for i in range(len(tuples)):
            column_names.add(tuples[i][3] + '.' + tuples[i][1])
            target_tables.add(tuples[i][3])

            for j in range(i + 1, len(tuples)):
                target_tables.add(tuples[j][3])
                if (tuples[i][3], tuples[j][3]) in self.direct_links:
                    link_clauses.add(
                        self.direct_links[(tuples[i][3], tuples[j][3])]
                    )
                elif (tuples[j][3], tuples[i][3]) in self.direct_links:
                    link_clauses.add(
                        self.direct_links[(tuples[j][3], tuples[i][3])]
                    )
                elif (tuples[i][3], tuples[j][3]) in self.non_direct_links:
                    mid_t = self.non_direct_links[(tuples[i][3], tuples[j][3])]

                    target_tables.add(mid_t)
                    if (tuples[i][3], mid_t) in self.direct_links:
                        link_clauses.add(
                            self.direct_links[(tuples[i][3], mid_t)]
                        )
                    elif (mid_t, tuples[i][3]) in self.direct_links:
                        link_clauses.add(
                            self.direct_links[(mid_t, tuples[i][3])]
                        )
                    else:
                        continue
                    prev_t = mid_t
                    while (prev_t, tuples[j][3]) not in self.direct_links and \
                            (tuples[j][3], prev_t) not in self.direct_links:
                        mid_t = self.non_direct_links[(mid_t, tuples[j][3])]
                        target_tables.add(mid_t)

                        if (prev_t, mid_t) in self.direct_links:
                            link_clauses.add(
                                self.direct_links[(prev_t, mid_t)]
                            )
                        else:
                            link_clauses.add(
                                self.direct_links[(mid_t, prev_t)]
                            )
                        prev_t = mid_t

                    if (prev_t, tuples[j][3]) in self.direct_links:
                        link_clauses.add(
                            self.direct_links[(prev_t, tuples[j][3])]
                        )
                    else:
                        link_clauses.add(
                            self.direct_links[(tuples[j][3], prev_t)]
                        )
                elif tuples[i][3] == tuples[j][3]:
                    continue
                else:
                    raise KeyError
        # print(column_names, target_tables, link_clauses, sep='\n')
        return column_names, target_tables, link_clauses, filter_clauses, order_statements

    def on_preview_clicked(self, *args):
        names, tables, links, filters, order = self.compose_query()

        if filters is not None:
            final = self.proxy.get_final_query(names, tables, links, filters, order)
            self.preview_text.set_text(final.as_string(self.proxy.connection))
            self.preview_window.show()

    def on_preview_closed(self, *args):
        self.preview_window.hide()
        return True

    def on_run_query_clicked(self, *args):
        for col in self.result_view.get_columns():
            self.result_view.remove_column(col)

        m = self.result_view.get_model()
        if m is not None:
            m.clear()
        self.result_view.set_model(None)

        names, tables, links, filters, order = self.compose_query()
        final = self.proxy.get_final_query(names, tables, links, filters, order)
        res = self.proxy.execute_select(final)

        j = 0
        for i in self.selected_columns:
            self.result_view.append_column(
                Gtk.TreeViewColumn(
                    title=i[0],
                    cell_renderer=Gtk.CellRendererText(),
                    text=j
                )
            )
            j += 1

        model = Gtk.ListStore(*([str]*len(self.selected_columns)))
        self.result_view.set_model(model)

        for r in res:
            str_r = [str(i) for i in r]
            model.append(str_r)

