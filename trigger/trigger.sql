-- ХП
CREATE TABLE spec
(
    id          INTEGER PRIMARY KEY,
    table_name  VARCHAR,
    column_name VARCHAR,
    max_val     INTEGER
);

INSERT INTO spec(id, table_name, column_name, max_val)
VALUES (1, 'spec', 'id', 1);

CREATE OR REPLACE FUNCTION spec_on_upsert()
    RETURNS TRIGGER
AS
$$
DECLARE
    tmp spec.max_val%TYPE;
BEGIN
    EXECUTE format('SELECT MAX(%I) FROM new', quote_ident(tg_argv[0])) INTO tmp;
        UPDATE spec
        SET max_val = tmp
        WHERE table_name = tg_table_name AND column_name = tg_argv[0] and tmp > max_val;

    RETURN NULL;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION find_new_id(n_table CHARACTER VARYING, n_column CHARACTER VARYING) RETURNS INTEGER
    LANGUAGE plpgsql
AS
$$
DECLARE
    __tmp_counter spec.max_val%TYPE;
BEGIN
    IF EXISTS(SELECT * FROM spec WHERE table_name = n_table AND column_name = n_column) THEN
        UPDATE spec SET max_val = max_val + 1 WHERE table_name = n_table AND column_name = n_column;
    ELSE
        EXECUTE format('SELECT MAX(%I) FROM %I', quote_ident(n_column), quote_ident(n_table)) INTO __tmp_counter;

        IF __tmp_counter IS NULL THEN
            EXECUTE format(
                    'CREATE TRIGGER %I_%I_insert '
                        'AFTER INSERT '
                        'ON %I '
                        'REFERENCING NEW TABLE AS new '
                        'FOR EACH STATEMENT '
                        'EXECUTE FUNCTION spec_on_upsert(%I)',
                    quote_ident(n_table), quote_ident(n_column), quote_ident(n_table), quote_ident(n_column));
            EXECUTE format(
                    'CREATE TRIGGER %I_%I_update '
                        'AFTER UPDATE '
                        'ON %I '
                        'REFERENCING NEW TABLE AS new '
                        'FOR EACH STATEMENT '
                        'EXECUTE FUNCTION spec_on_upsert(%I)',
                    quote_ident(n_table), quote_ident(n_column), quote_ident(n_table), quote_ident(n_column));
            __tmp_counter = 0;
        END IF;

        INSERT INTO spec (id, table_name, column_name, max_val)
        VALUES (find_new_id('spec', 'id'), n_table, n_column, __tmp_counter + 1);
    END IF;

    RETURN (SELECT max_val FROM spec WHERE TABLE_NAME = n_table AND COLUMN_NAME = n_column);
END;
$$;

CREATE TABLE test (
    id int,
    name varchar
);

INSERT INTO test(id, name) VALUES (find_new_id('test', 'id'), 'Oh Hello There');
SELECT * FROM spec;
INSERT INTO test(id, name) VALUES (25, 'triggering insert trigger'),
                                  (55, 'triggering insert trigger'),
                                  (32, 'triggering insert trigger');
SELECT find_new_id('test', 'id');
SELECT * FROM test;
SELECT * FROM spec;

UPDATE test
SET id = id + 1000, name = 'triggering update trigger'
WHERE id > 30;

SELECT * FROM test;
SELECT * FROM spec;


CREATE TABLE test_trigger (
    id int,
    name varchar,
    id2 int
);

INSERT INTO test_trigger(id, name, id2)
VALUES (find_new_id('test_trigger', 'id'), 'he-he-hello', find_new_id('test_trigger', 'id2'));

SELECT find_new_id('test_trigger', 'id');
SELECT find_new_id('test_trigger', 'id2');

SELECT * FROM spec;

INSERT INTO test_trigger(id, name, id2)
VALUES (find_new_id('test_trigger', 'id'), 'hello1', 999),
       (find_new_id('test_trigger', 'id'), 'hello2', 111);

INSERT INTO test_trigger(id, name, id2)
VALUES (9060, 'testsad11', find_new_id('test_trigger', 'id2')),
       (100500, 'very sad', 700);

SELECT * FROM spec;
SELECT * FROM test_trigger;

UPDATE test_trigger
SET id = id + 5000, id2 = id2 + 90500, name = 'TRIGGERED'
WHERE id > 15 OR id2 < 500;

SELECT * FROM test_trigger;
SELECT * FROM test;
SELECT * FROM spec;

DROP TABLE test;
DROP TABLE test_trigger;
DROP TABLE spec;
DROP FUNCTION find_new_id(n_table CHARACTER VARYING, n_column CHARACTER VARYING);
