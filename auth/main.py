import gi
import psycopg2
import psycopg2.extras
import datetime
import re
import bcrypt

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from zxcvbn import zxcvbn
from os import getenv


# TODO: add custom signal
# TODO: use separate thread for database querying
class AuthWindow:
    """Main program window"""

    def __init__(self, db_connection_str):
        self.db_connection_str = db_connection_str

        self.builder = Gtk.Builder()
        self.builder.add_from_file("window.ui")

        self.handlers = {
            "on_destroy": self.on_destroy,
            "on_error_response": self.on_error_response,
            "on_login_entry_changed": self.on_login_entry_changed,
            "on_password_entry_changed": self.on_password_entry_changed,
            "on_auth_button_clicked": self.on_auth_button_clicked
        }
        self.builder.connect_signals(self.handlers)

        self.infobar = self.builder.get_object("error_info")
        self.login_entry = self.builder.get_object("login_entry")
        self.password_entry = self.builder.get_object("password_entry")
        self.infobar_message = self.builder.get_object("error_label")
        self.spinner = self.builder.get_object("progress_spinner")
        self.auth_button = self.builder.get_object("auth_button")
        self.status_icon = self.builder.get_object("status_icon")

        self.login_is_valid = False
        self.pass_is_good = False

        self.infobar_message.set_text('Whitespaces will be trimmed')
        self.infobar.set_revealed(True)

    def on_destroy(self, *args):
        Gtk.main_quit()

    def on_error_response(self, *args):
        self.infobar.set_revealed(False)

    def on_login_entry_changed(self, *args):
        self.login_is_valid = False
        self.auth_button.set_sensitive(False)
        if not self.infobar_message.get_text() == 'Password is too weak!':
            self.infobar.set_revealed(False)

        if self.login_entry.get_text() == '':
            return

        # self.spinner.start()

        if not re.match('[`a-zа-я #!@$%^&*-+=\\|/?.,><~№\"\';:\[\]()_0-9]',
                        self.login_entry.get_text(),
                        flags=re.IGNORECASE):
            self.infobar_message.set_text("Invalid login")
            self.infobar.set_revealed(True)
            self.auth_button.set_sensitive(False)

            return

        with psycopg2.connect(self.db_connection_str) as conn:
            trimmed_login = re.sub(' +', ' ', self.login_entry.get_text())
            trimmed_login = trimmed_login.strip()
            self.auth_button.set_label("Register")
            self.auth_button.set_sensitive(False)
            cur = conn.cursor()
            cur.execute(
                'SELECT * '
                'FROM users.users '
                'WHERE login = %s', (trimmed_login,))

            if cur.fetchone() is None:
                # self.auth_button.set_sensitive(True)
                self.spinner.stop()
                self.status_icon.set_from_icon_name('gtk-ok', Gtk.IconSize.BUTTON)
                self.login_is_valid = True
            else:
                self.infobar_message.set_text("Such user already exists!")
                self.auth_button.set_label("Log in")
                self.auth_button.set_sensitive(True)
                self.infobar.set_revealed(True)
            cur.close()
        conn.close()

        if self.password_entry.get_text() == '':
            self.auth_button.set_sensitive(False)
        if self.pass_is_good:
            self.auth_button.set_sensitive(True)

    def on_password_entry_changed(self, *args):
        self.infobar.set_revealed(False)
        self.pass_is_good = False

        if self.password_entry.get_text() == '':
            self.auth_button.set_sensitive(False)
            return

        self.auth_button.set_sensitive(True)
        if self.auth_button.get_label() == "Register":
            if zxcvbn(self.password_entry.get_text(),
                      self.login_entry.get_text())['score'] > 2 and self.login_is_valid:
                self.auth_button.set_sensitive(True)
                self.infobar.set_revealed(False)
                self.pass_is_good = True
            else:
                self.infobar_message.set_text("Password is too weak!")
                self.auth_button.set_sensitive(False)
                self.infobar.set_revealed(True)

    def on_auth_button_clicked(self, *args):
        main_box = self.builder.get_object("main_box")
        main_box.set_sensitive(False)

        salt = bcrypt.gensalt()
        hashed_pw = bcrypt.hashpw(self.password_entry.get_text().encode('utf-8'), salt)

        # self.spinner.start()
        with psycopg2.connect(self.db_connection_str, cursor_factory=psycopg2.extras.DictCursor) as conn:
            trimmed_login = re.sub(' +', ' ', self.login_entry.get_text())
            trimmed_login = trimmed_login.strip()

            cur = conn.cursor()
            if self.auth_button.get_label() == "Register":
                cur.execute(
                    'INSERT INTO users.users (login, password, reg_date) '
                    'VALUES (%s, %s, %s)', (trimmed_login,
                                            hashed_pw.decode('utf-8'),
                                            datetime.datetime.utcnow())
                )
                self.infobar_message.set_text("Registered successfully!")
                self.infobar.set_revealed(True)
                self.auth_button.set_label("Log in")
                # self.auth_button.set_sensitive(False)
            else:
                cur.execute(
                    'SELECT password '
                    'FROM users.users '
                    'WHERE login = %s', (trimmed_login,)
                )

                if bcrypt.checkpw(self.password_entry.get_text().encode('utf-8'),
                                  cur.fetchone()['password'].encode('utf-8')):
                    self.infobar_message.set_text("Logged in successfully!")
                    self.pass_is_good = False
                    self.login_is_valid = False
                    self.login_entry.set_text('')
                    self.password_entry.set_text('')
                else:
                    self.infobar_message.set_text("Invalid password")

                self.infobar.set_revealed(True)
                self.spinner.stop()
            cur.close()
        conn.close()

        main_box.set_sensitive(True)


def main():
    db_user = getenv('DB_USER')
    db_name = getenv('DB_NAME')

    if db_user is None or db_name is None:
        print('None envvars set, exiting')
        exit(1)

    db_str = 'host=localhost user=' + db_user + ' dbname=' + db_name

    app = AuthWindow(db_str)
    window = app.builder.get_object('AuthenticatorWindow')
    window.show_all()

    Gtk.main()


if __name__ == "__main__":
    main()
