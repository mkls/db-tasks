import bcrypt
import gi
import psycopg2
import psycopg2.extras
import re

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from os import getenv
from datetime import datetime, date
from zxcvbn import zxcvbn


class User:
    def __init__(self, login, reg_date):
        self.__login = login
        self.__reg_date = reg_date

    def get_login(self):
        return self.__login

    def set_login(self, new_login: str):
        self.__login = new_login

    def get_reg_date(self):
        return self.__reg_date

    def set_reg_date(self, reg_date):
        self.__reg_date = reg_date


class CrudWindow:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file('crud.glade')

        handlers = {
            'on_destroy': Gtk.main_quit,
            'on_new_item_button_clicked': self.__new_item_clicked,
            'on_remove_clicked': self.__on_remove_clicked,
            'on_dialog_infobar_response': self.__infobar_response,
            'on_row_activated': self.__row_activated,
            'on_cancel_button_clicked': self.__on_cancel_clicked,
            'on_credentials_changed': self.__on_credentials_changed,
            'on_confirm_clicked': self.__on_confirm_clicked
        }

        builder.connect_signals(handlers)

        db_user = getenv('DB_USER')
        db_name = getenv('DB_NAME')

        self.__connection_string = 'host=localhost user=' + db_user + ' dbname=' + db_name

        # It's a treeView instance actually
        self.__list_view = builder.get_object('list_view')
        self.__headerbar = builder.get_object('headerbar')
        self.__list_selection = builder.get_object('selection')
        self.__window = builder.get_object('application_window')
        self.__infobar = builder.get_object('dialog_infobar')
        self.__confirm_dialog = builder.get_object('add_edit_window')
        self.__new_item_button = builder.get_object('new_item_button')
        self.__remove_items_button = builder.get_object('remove_items_button')
        self.__login_entry = builder.get_object('login_entry')
        self.__password_entry = builder.get_object('password_entry')
        self.__confirm_button = builder.get_object('confirm_button')
        self.__login_cell_renderer = builder.get_object('login_renderer')
        self.__date_cell_renderer = builder.get_object('reg_date_renderer')
        self.__login_column = builder.get_object('login_column')
        self.__reg_date_column = builder.get_object('reg_date_column')
        self.__users = builder.get_object('list_store')
        self.__infobar_label = builder.get_object('dialog_infobar_label')

        self.__spin_day = builder.get_object('spin_day')
        self.__spin_month = builder.get_object('spin_month')
        self.__spin_year = builder.get_object('spin_year')

        self.__confirm_dialog.set_attached_to(self.__window)

        self.__password_set = False
        self.__login_set = False
        self.__editing = False

        self.__login_column.set_cell_data_func(self.__login_cell_renderer, self.__render_login)
        self.__reg_date_column.set_cell_data_func(self.__date_cell_renderer, self.__render_date)

        with psycopg2.connect(self.__connection_string) as conn:
            with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                cur.execute('''SELECT login, reg_date
                               FROM users.users''')

                for user in cur.fetchall():
                    u = User(user['login'], user['reg_date'])
                    self.__users.append((u,))
        conn.close()

        if self.__users.iter_n_children(None) > 0:
            self.__remove_items_button.set_sensitive(True)

    def show(self):
        self.__window.show_all()

    def __new_item_clicked(self, *args):
        self.__login_entry.set_text('')
        self.__password_entry.set_text('')

        self.__editing = False

        self.__confirm_dialog.set_title('New user')
        self.__confirm_dialog.show()

    def __on_remove_clicked(self, *args):
        (model, items) = self.__list_selection.get_selected_rows()

        iters = [self.__users.get_iter(i) for i in items]
        with psycopg2.connect(self.__connection_string) as conn:
            with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                for i in iters:
                    login = self.__users.get_value(i, 0).get_login()
                    cur.execute('''DELETE FROM users.users
                                   WHERE login = %s''', (login,))
                    self.__users.remove(i)
        conn.close()

    def __infobar_response(self, *args):
        self.__infobar.set_revealed(False)

    def __on_cancel_clicked(self, *args):
        self.__confirm_dialog.hide()

    def __row_activated(self, treeview, path, view_column, *args):
        iter = self.__users.get_iter(path)
        user = self.__users.get_value(iter, 0)

        self.__spin_day.set_value(user.get_reg_date().day)
        self.__spin_month.set_value(user.get_reg_date().month)
        self.__spin_year.set_value(user.get_reg_date().year)

        self.__editing = True

        self.__login_entry.set_text(user.get_login())
        self.__password_entry.set_text('')

        self.__confirm_dialog.set_title('Edit')

        self.__confirm_dialog.show()

    def __render_login(self, column, cell, model, iter, user_data):
        user = self.__users.get_value(iter, 0)
        cell.set_property('text', user.get_login())

    def __render_date(self, column, cell, model, iter, user_data):
        user = self.__users.get_value(iter, 0)
        cell.set_property('text', user.get_reg_date().strftime('%Y-%m-%d'))

    def __on_credentials_changed(self, *args):
        pass_is_valid = False

        if self.__login_entry.get_text() == '':
            self.__update_infobar('Login can not be empty', True)
            self.__login_set = False
        elif not re.match(r'^[`a-zа-я #!@$%^&*\-+=\\|/?.,><~№\"\';:\[\]()_0-9]+$',
                          self.__login_entry.get_text(),
                          flags=re.IGNORECASE):
            self.__update_infobar('Login contains forbidden symbols', True)
            self.__login_set = False
        else:
            self.__update_infobar('', False)
            self.__login_set = True

        if self.__password_entry.get_text() != '':
            if zxcvbn(self.__password_entry.get_text(),
                      self.__login_entry.get_text())['score'] < 2:
                self.__update_infobar('Password is too weak!', True)
                self.__password_set = False
            else:
                self.__update_infobar('Password is good!', False)
                self.__password_set = True
        elif self.__editing:
            self.__password_set = True
        else:
            self.__password_set = False

        if self.__login_set and self.__password_set:
            self.__confirm_button.set_sensitive(True)
        else:
            self.__confirm_button.set_sensitive(False)

    def __on_confirm_clicked(self, *args):
        with psycopg2.connect(self.__connection_string) as conn:
            with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                new_password = self.__password_entry.get_text().encode('utf-8')
                # new_login = self.__login_entry.get_text()
                new_login = re.sub(' +', ' ', self.__login_entry.get_text())
                new_login = new_login.strip()

                day = int(self.__spin_day.get_value())
                month = int(self.__spin_month.get_value())
                year = int(self.__spin_year.get_value())

                new_date = datetime.strptime(str(day) + '-' + str(month) + '-' + str(year), '%d-%m-%Y')

                salt = bcrypt.gensalt(12)
                hash = bcrypt.hashpw(new_password, salt).decode('utf-8')

                if self.__editing:
                    # As it's impossible to double-click on multiple rows at the same time, items contains only 1 row
                    (model, items) = self.__list_selection.get_selected_rows()
                    iter = self.__users.get_iter(items[0])
                    user = self.__users.get_value(iter, 0)

                    try:
                        if self.__password_entry.get_text() == '':
                            cur.execute('UPDATE users.users SET login = %s, reg_date = %s WHERE login = %s',
                                        (new_login, new_date, user.get_login()))
                        else:
                            cur.execute('UPDATE users.users SET login = %s, password = %s, reg_date = %s WHERE login = %s',
                                        (new_login, hash, new_date, user.get_login()))
                        user.set_login(new_login)
                        user.set_reg_date(new_date)

                    except psycopg2.IntegrityError:
                        self.__update_infobar('User with such login already exists', True)
                        return
                else:
                    try:
                        cur.execute('INSERT INTO users.users VALUES (%s, %s, %s)',
                                    (new_login, hash, new_date))
                    except psycopg2.IntegrityError:
                        self.__update_infobar('Seems like such user already exists, try again', True)
                        return
                    iter = self.__users.append((User(new_login, new_date),))
                    user = self.__users.get_value(iter, 0)
                print(cur.query)

        conn.close()
        self.__confirm_dialog.hide()

    def __update_infobar(self, text: str, revealed: bool):
        self.__infobar_label.set_text(text)
        self.__infobar.set_revealed(revealed)
