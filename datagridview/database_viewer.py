import gi
import re

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from db_connector import DbConnector


class Viewer:
    def __init__(self, connector: DbConnector):
        builder = Gtk.Builder()
        builder.add_from_file('dgv.glade')

        self.window = builder.get_object('app_window')
        self.developers = builder.get_object('developer')
        self.dev_types = builder.get_object('dev_type')
        self.countries = builder.get_object('country')
        self.os_versions = builder.get_object('os_version')
        self.os_names = builder.get_object('os_name')

        self.add_row_button = builder.get_object('add_row_button')
        self.remove_row_button = builder.get_object('remove_row_button')

        self.os_name_column = builder.get_object('os_name_column')
        self.country_column = builder.get_object('country_column')
        self.os_dev_column = builder.get_object('os_dev_column')
        self.dev_type_column = builder.get_object('dev_type_column')

        self.notebook = builder.get_object('notebook')
        self.info_bar = builder.get_object('info_bar')
        self.info_bar_label = builder.get_object('info_bar_label')

        self.reselection = False

        self.chosen_dev = None

        self.columns = {
            'name': 0,
            'version': 1,
            'os_dev_name': 2,
            'devv_id': 0,
            'dev_name': 1,
            'country': 4,
            'dev_type': 5,
            'os_id': 3,
            'dev_id': 4,
            'dev_type_id': 3,
            'country_id': 2,
            'type_id': 0,
            'type': 1,
            'c_id': 0,
            'c_name': 1,
            'os_name_id': 0,
            'os_name': 1
        }

        self.os_name_renderer = builder.get_object('os_name_renderer')
        self.os_version_renderer = builder.get_object('version_renderer')
        self.os_dev_renderer = builder.get_object('os_dev_renderer')
        self.dev_name_renderer = builder.get_object('dev_name_renderer')
        self.country_renderer = builder.get_object('country_renderer')
        self.dev_type_renderer = builder.get_object('dev_type_renderer')
        self.os_view = builder.get_object('os_view')
        self.dev_view = builder.get_object('dev_view')
        self.os_selection = builder.get_object('os_selection')
        self.dev_selection = builder.get_object('dev_selection')
        self.insert_button = builder.get_object('add_row_button')

        self.os_name_renderer.connect('edited',
                                      self.on_text_entry_updated,
                                      self.columns['name'])
        self.os_name_renderer.connect('changed',
                                      self.combo_changed)
        self.os_version_renderer.connect('edited',
                                         self.on_text_entry_updated,
                                         self.columns['version'])
        self.os_dev_renderer.connect('edited',
                                     self.on_text_entry_updated,
                                     self.columns['os_dev_name'])
        self.os_dev_renderer.connect('changed',
                                     self.combo_changed)
        self.dev_name_renderer.connect('edited',
                                       self.on_text_entry_updated,
                                       self.columns['dev_name'])
        self.country_renderer.connect('edited',
                                      self.on_text_entry_updated,
                                      self.columns['country'])
        self.dev_type_renderer.connect('edited',
                                       self.on_text_entry_updated,
                                       self.columns['dev_type'])

        handlers = {
            'on_destroy': Gtk.main_quit,
            'on_text_entry_edited': self.on_text_entry_updated,
            'on_insert_clicked': self.on_insert_clicked,
            'on_selection_changed': self.selection_changed,
            'on_delete_row_clicked': self.delete_clicked
        }

        self.last_updated_row = None
        self.operation = 0

        self.adapter = connector
        if self.adapter is not None:
            self.load_from_db()

        builder.connect_signals(handlers)

    # :( That was so short and beautiful
    def __load_data(self, items, target):
        for item in items:
            target.append(item)

    def __load_os_names(self, names: list):
        for name in names:
            self.os_names.append((name['id'],
                                  name['name']))

    def __load_developer_types(self, types: list):
        for dev_type in types:
            self.dev_types.append((dev_type['id'],
                                   dev_type['name']))

    def __load_developers(self, developers: list):
        for developer in developers:
            self.developers.append((developer['id'],
                                    developer['name'],
                                    developer['country'],
                                    developer['type']))

    def __load_countries(self, countries: list):
        for country in countries:
            self.countries.append((country['id'],
                                   country['name']))

    def reload_os_versions(self):
        os_versions = self.adapter.run_query('fetch_os_version')
        self.os_versions.clear()
        self.__load_data(os_versions, self.os_versions)

    def __load_os_versions(self, os_versions: list):
        for version in os_versions:
            self.os_versions.append((version['os_name'],
                                     version['version'],
                                     version['dev_name']))

    def load_from_db(self):
        names = self.adapter.run_query('fetch_os_names')
        dev_types = self.adapter.run_query('fetch_dev_types')
        countries = self.adapter.run_query('fetch_countries')
        developers = self.adapter.run_query('fetch_developers')
        os_versions = self.adapter.run_query('fetch_os_version')

        self.load_data(names, dev_types, developers, countries, os_versions)

    def add_new_os(self, name: str):
        new_index = self.adapter.marshal_query('INSERT INTO os.os_name (name) '
                                               'VALUES (%s) '
                                               'RETURNING id',
                                               (name,))
        t_iter = self.os_names.append()
        self.os_names.set_value(t_iter, 0, new_index[0][0])
        self.os_names.set_value(t_iter, 1, name)

        return new_index[0][0]

    def load_data(self,
                  names: list,
                  types: list,
                  developers: list,
                  countries: list,
                  os_versions: list):
        self.__load_data(countries, self.countries)
        self.__load_data(types, self.dev_types)
        self.__load_data(names, self.os_names)
        self.__load_data(developers, self.developers)
        self.__load_data(os_versions, self.os_versions)

    def show_all(self):
        self.window.show_all()

    def on_text_entry_updated(self, cell, path, new_text, column):
        trimmed_text = re.sub(' +', ' ', new_text)
        trimmed_text = trimmed_text.strip()

        # if (column == self.columns['name'] or column == self.columns['dev_name']) \
        #         and trimmed_text == '':
        #     return

        # if self.notebook.get_current_page() == 0 and \
        #         self.os_versions[path][column] == trimmed_text:
        #     return
        # elif self.notebook.get_current_page() == 1 and \
        #         self.developers[path][column] == trimmed_text:
        #     return

        if self.notebook.get_current_page() == 0:
            self.last_updated_row = self.os_versions.get_iter(path)
            self.os_versions[path][column] = trimmed_text
        else:
            self.last_updated_row = self.developers.get_iter(path)
            self.developers[path][column] = trimmed_text
        if self.operation == 0:
            self.operation = 1
            self.sync_buttons()

    def update_buttons(self):
        current_page = self.notebook.get_current_page()
        if current_page == 0 and len(self.os_versions) == 0:
            self.remove_row_button.set_sensitive(False)
        elif len(self.developers) == 0:
            self.remove_row_button.set_sensitive(False)

    def find_id(self, target, id_column, name_column, source):
        if target is None:
            return None
        for t_id, name in [(x[id_column], x[name_column]) for x in source]:
            if name == target:
                return t_id
        return -1

    def combo_changed(self, combo, str_path, new_iter, *args):
        # path = self.developers.get_path(str_path)
        self.last_updated_row = self.os_versions.get_iter(str_path)
        self.chosen_dev = new_iter
        self.os_versions[self.last_updated_row][self.columns['dev_id']] = \
            self.developers[new_iter][self.columns['devv_id']]
        self.os_versions[self.last_updated_row][self.columns['os_dev_name']] = \
            self.developers[new_iter][self.columns['dev_name']]
        print(self.os_versions[self.last_updated_row][self.columns['dev_id']])
        if self.operation != 2:
            self.operation = 1
        self.sync_buttons()
        self.update_tabs(self.notebook)

    '''Find duplicate @item in @source by specifying @columns for comparison'''

    def find_duplicate(self, source, item, iter, *columns):
        start_iter = source.get_iter_first()
        while start_iter is not None:
            counter = 0
            if source.get_path(start_iter) == source.get_path(iter):
                start_iter = source.iter_next(start_iter)
                continue
            for i in columns:
                if source[start_iter][i] != item[i] and \
                        (source[start_iter][i] != 0 and
                         item[i] != 0):
                    break
                counter += 1
            if counter == len(columns):
                return True
            start_iter = source.iter_next(start_iter)
        return False

    def sync_buttons(self):
        self.add_row_button.set_sensitive(True)
        self.remove_row_button.set_sensitive(True)

        if self.operation == 1:
            self.add_row_button.set_sensitive(False)
            self.remove_row_button.set_sensitive(False)
        elif self.operation == 2:
            self.add_row_button.set_sensitive(False)

    def sync_info_bar(self, msg: str, show: bool):
        self.info_bar_label.set_text(msg)
        self.info_bar.set_revealed(show)

    def selection_changed(self, selection):
        if self.reselection:
            self.reselection = False
            return

        self.update_buttons()
        current_page = self.notebook.get_current_page()
        if self.last_updated_row is not None:
            not_filled = False

            if current_page == 0:
                row = self.os_versions[self.last_updated_row]
                if row[0] is None or row[1] is None:
                    not_filled = True
            else:
                row = self.developers[self.last_updated_row]
                if row[self.columns['dev_name']] is None:
                    not_filled = True

            if not_filled:
                self.sync_info_bar('Please, fill in the data', True)
                # self.reselection = True
                selection.select_iter(self.last_updated_row)
            else:
                if current_page == 0:
                    last_row = self.os_versions[self.last_updated_row]
                    os_name = last_row[0]
                    version = last_row[1]
                    dev_name = last_row[2]
                    old_id = last_row[3]
                    old_version = last_row[5]
                    dev_id = last_row[self.columns['dev_id']]

                    os_id = self.find_id(os_name,
                                         0,
                                         1,
                                         self.os_names)
                    # if dev_name != '' and dev_name is not None:
                    #     dev_id = self.find_id(dev_name,
                    #                           0,
                    #                           1,
                    #                           self.developers)
                    # if dev_id == -1:
                    #     dev_id = None
                    if dev_id is None or dev_id == 0:
                        dev_id = None

                    counter = 0
                    if os_id == -1:
                        for name_row in self.os_versions:
                            if name_row[self.columns['os_id']] == old_id:
                                counter += 1
                        if counter > 1:
                            os_id = self.add_new_os(os_name)
                        else:
                            self.adapter.query_no_return('UPDATE os.os_name '
                                                         'SET name = %s '
                                                         'WHERE id = %s',
                                                         os_name, old_id)
                            for name_row in self.os_names:
                                if name_row[self.columns['os_name_id']] == old_id:
                                    name_row[self.columns['os_name']] = os_name
                                    break
                            os_id = old_id

                    entry = (os_id, version, dev_id)
                    if self.find_duplicate(self.os_versions,
                                           (os_name, version, dev_id, os_id),
                                           self.last_updated_row,
                                           3,
                                           1):
                        # self.reselection = True
                        self.os_selection.select_iter(self.last_updated_row)
                        self.sync_info_bar('Such os version already exists',
                                           True)
                        return

                    if self.operation == 2:
                        self.os_versions.set_value(self.last_updated_row,
                                                   3,
                                                   os_id)
                        self.os_versions.set_value(self.last_updated_row,
                                                   4,
                                                   dev_id)
                        self.adapter.query_no_return('INSERT INTO os.os_version'
                                                     '(os_id, version, dev_id) '
                                                     'VALUES (%s, %s, %s)',
                                                     *entry)
                    elif self.operation == 1:
                        self.adapter.query_no_return('UPDATE os.os_version '
                                                     'SET os_id = %s,'
                                                     'version = %s,'
                                                     'dev_id = %s '
                                                     'WHERE os_id = %s AND version = %s',
                                                     *entry, old_id, old_version)
                    last_row[5] = version
                    last_row[3] = os_id
                else:
                    row = self.developers[self.last_updated_row]
                    dev_id = row[self.columns['devv_id']]
                    dev_name = row[self.columns['dev_name']]
                    country_id = row[self.columns['country_id']]
                    type_id = row[self.columns['dev_type_id']]
                    country_name = row[self.columns['country']]
                    type_name = row[self.columns['dev_type']]

                    if country_name is not None and country_name != '':
                        country_id = self.find_id(country_name,
                                                  self.columns['c_id'],
                                                  self.columns['c_name'],
                                                  self.countries)
                    else:
                        country_id = None
                    if country_id == -1:
                        country_id = self.add_new_country(country_name)
                    row[self.columns['country_id']] = country_id
                    row[self.columns['country']] = country_name

                    if type_name is not None and type_name != '':
                        type_id = self.find_id(type_name,
                                               self.columns['type_id'],
                                               self.columns['type'],
                                               self.dev_types)
                    if type_id is None or type_id == 0:
                        type_id = None
                    elif type_id == -1:
                        type_id = self.add_new_type(type_name)
                    row[self.columns['type_id']] = type_id

                    if self.find_duplicate(self.developers,
                                           (0, dev_name, country_id, type_id),
                                           self.last_updated_row,
                                           self.columns['dev_name'],
                                           self.columns['country_id'],
                                           self.columns['dev_type_id']):
                        # self.reselection = True
                        self.dev_selection.select_iter(self.last_updated_row)
                        self.sync_info_bar('Such developer already exists',
                                           True)
                        return

                    if self.operation == 1:
                        for os_row in self.os_versions:
                            if os_row[self.columns['dev_id']] == dev_id:
                                os_row[self.columns['os_dev_name']] = dev_name
                        self.adapter.query_no_return('UPDATE os.developer '
                                                     'SET '
                                                     '    name = %s,'
                                                     '    country = %s,'
                                                     '    type = %s '
                                                     'WHERE id = %s',
                                                     dev_name,
                                                     country_id,
                                                     type_id,
                                                     dev_id)
                    elif self.operation == 2:
                        t_id = self.adapter.marshal_query('INSERT INTO os.developer '
                                                          '(name, type, country) '
                                                          'VALUES (%s, %s, %s) '
                                                          'RETURNING id ',
                                                          dev_name,
                                                          type_id,
                                                          country_id)[0][0]
                        row[self.columns['devv_id']] = t_id

                self.last_updated_row = None
                self.operation = 0
                self.sync_info_bar('', False)
                self.sync_buttons()

        self.update_tabs(self.notebook)

    def on_insert_clicked(self, *args):
        if self.last_updated_row is not None:
            return

        self.operation = 2
        if self.notebook.get_current_page() == 0:
            self.last_updated_row = self.os_versions.append()
            self.os_selection.select_iter(self.last_updated_row)
        else:
            self.last_updated_row = self.developers.append()
            self.dev_selection.select_iter(self.last_updated_row)

        self.sync_buttons()

    def update_tabs(self, notebook):
        status = True
        if self.last_updated_row is not None:
            status = False

        notebook.set_show_tabs(status)

    def delete_clicked(self, *args):
        self.last_updated_row = None
        if self.notebook.get_current_page() == 0:
            model, item = self.os_selection.get_selected()
            if self.operation == 0:
                self.adapter.query_no_return('''DELETE FROM os.os_version 
                                          WHERE os_id = %s AND version = %s''',
                                             model[item][3],
                                             model[item][1])
            self.os_versions.remove(item)
        else:
            model, item = self.dev_selection.get_selected()
            if self.operation == 0:
                target_dev_id = model[item][self.columns['devv_id']]
                self.adapter.query_no_return('DELETE FROM os.developer '
                                             'WHERE id = %s',
                                             target_dev_id)
                for row in self.os_versions:
                    if row[self.columns['dev_id']] == target_dev_id:
                        row[self.columns['dev_id']] = None
                        row[self.columns['os_dev_name']] = None
            self.developers.remove(item)

        self.operation = 0
        self.sync_info_bar('', False)
        self.sync_buttons()

    def add_new_country(self, country_name):
        if country_name is None or country_name == '':
            return None
        new_index = self.adapter.marshal_query('INSERT INTO os.country (name) '
                                               'VALUES (%s) '
                                               'RETURNING id',
                                               (country_name,))
        t_iter = self.os_names.append()
        self.countries.set_value(t_iter, self.columns['country_id'], new_index)
        self.countries.set_value(t_iter, self.columns['country'], country_name)

        return new_index[0][0]

    def add_new_type(self, type_name):
        if type_name is None or type_name == '':
            return None
        new_index = self.adapter.marshal_query('INSERT INTO os.developer_type (name) '
                                               'VALUES (%s) '
                                               'RETURNING id',
                                               (type_name,))
        t_iter = self.dev_types.append()
        self.dev_types.set_value(t_iter, self.columns['type_id'], new_index[0][0])
        self.dev_types.set_value(t_iter, self.columns['type'], type_name)

        return new_index[0][0]
