import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from database_viewer import Viewer
from postgres_connector import PostgresConnector
from os import getenv

queries = {
    'fetch_os_version': 'SELECT os_name.name as os_name, '
                        '       version,'
                        '       developer.name as dev_name,'
                        '       os_id,'
                        '       dev_id,'
                        '       version '
                        'FROM operating_systems.os.os_version '
                        'LEFT JOIN '
                        ' operating_systems.os.os_name ON os_version.os_id = os_name.id '
                        'LEFT JOIN '
                        ' operating_systems.os.developer ON os_version.dev_id = developer.id',
    'fetch_dev_types': 'SELECT id, name '
                       'FROM operating_systems.os.developer_type',
    'fetch_countries': 'SELECT id, name '
                       'FROM operating_systems.os.country',
    'fetch_developers': 'SELECT developer.id,'
                        '       developer.name,'
                        '       country.id as country_id,'
                        '       developer_type.id as type_id,'
                        '       country.name as country_name,'
                        '       developer_type.name as dev_type_name '
                        'FROM operating_systems.os.developer'
                        '     LEFT JOIN'
                        '   operating_systems.os.country ON developer.country = country.id'
                        '     LEFT JOIN'
                        '   operating_systems.os.developer_type ON developer.type = developer_type.id',
    'fetch_os_names': 'SELECT id, name '
                      'FROM operating_systems.os.os_name',
}


def main():
    user = getenv('DB_USER')
    password = getenv('DB_PASS')
    dbname = getenv('DB_NAME')

    connector = PostgresConnector(user, dbname, password, queries)
    viewer = Viewer(connector)
    viewer.show_all()

    Gtk.main()


if __name__ == '__main__':
    main()
