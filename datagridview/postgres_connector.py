import psycopg2
from db_connector import DbConnector


class PostgresConnector(DbConnector):
    def __init__(self, user: str, db_name: str, password: str, queries: dict):
        self.user = user
        self.db_name = db_name
        self.password = password
        self.queries = queries
        self.connection = psycopg2.connect(user=self.user,
                                           dbname=self.db_name,
                                           password=self.password)
        self.cursor = self.connection.cursor()

    def __del__(self):
        self.cursor.close()
        self.connection.close()

    def query_no_return(self, query: str, *params):
        if params is None:
            self.cursor.execute(query)
        else:
            self.cursor.execute(query, params)
        self.connection.commit()
        print(self.cursor.query)

    def marshal_query(self, query: str, *params):
        result = None

        if params is None:
            self.cursor.execute(query)
        else:
            self.cursor.execute(query, params)

        result = self.cursor.fetchall()
        print(self.cursor.query)
        self.connection.commit()
        return result

    def run_query(self, name: str, *params):
        return self.marshal_query(self.queries[name], params)
