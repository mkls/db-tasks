from abc import ABC, abstractmethod


class DbConnector(ABC):
    @abstractmethod
    def marshal_query(self, query: str, *params):
        return

    @abstractmethod
    def query_no_return(self, query: str, *params):
        return

    @abstractmethod
    def run_query(self, name: str, *params):
        return
