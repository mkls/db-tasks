#!/usr/bin/python

import gi
import sqlite3
from sys import argv
from contextlib import closing

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class DbViewer(Gtk.Window):
    """Main window class"""

    def __init__(self):
        Gtk.Window.__init__(self, title="DB Viewer")
        self.set_border_width(5)
        self.set_default_size(400, 600)

        self.tree_store = Gtk.TreeStore(str)

        renderer = Gtk.CellRendererText()
        name_column = Gtk.TreeViewColumn("Name", renderer, text=0)

        self.tree_view = Gtk.TreeView.new_with_model(self.tree_store)
        self.tree_view.append_column(name_column)
        self.tree_view.connect("row-expanded", self.__on_expand)
        self.tree_view.connect("row-collapsed", self.__on_collapse)
        self.add(self.tree_view)

    def set_from_db(self, db_name: str) -> None:
        with closing(sqlite3.connect(db_name)) as connection:
            connection.row_factory = sqlite3.Row
            cursor = connection.cursor()
            try:
                cursor.execute('''
                    SELECT city.id as c_id,
                           city.name as city,
                           manufacturer.id as m_id,
                           manufacturer.name as manufacturer,
                           weapon.id as w_id,
                           weapon.name as weapon
                    FROM city
                            LEFT JOIN
                         manufacturer ON city.id = manufacturer.city
                            LEFT JOIN
                         weapon ON manufacturer.id = weapon.manufacturer
                    ORDER BY c_id, city, m_id, manufacturer''')
            except sqlite3.OperationalError as err:
                print("Failed to retrieve data from database. {}.\nCheck database file path.".format(err))
                cursor.close()
                connection.close()
                exit(1)

            parent = self.tree_store.get_iter_first()
            last_city_id = ""
            last_manufacturer_id = ""
            last_weapon_id = ""
            for row in cursor:
                if row["c_id"] is not None and row["c_id"] != last_city_id:
                    parent = self.tree_store.append(None, [row["city"]])
                    last_city_id = row["c_id"]

                if row["m_id"] is not None and row["m_id"] != last_manufacturer_id:
                    child = self.tree_store.append(parent, [row["manufacturer"]])
                    last_manufacturer_id = row["m_id"]

                if row["weapon"] is not None and row["w_id"] != last_weapon_id:
                    self.tree_store.append(child, [row["weapon"]])
                    last_weapon_id = row["w_id"]
            cursor.close()
            connection.close()

    def __on_expand(self, tree_widget, tree_iter, path):
        if self.tree_store.iter_parent(tree_iter) is not None:
            return

        children_with_children = 0
        children_without_children = 0

        child = self.tree_store.iter_children(tree_iter)
        while child is not None:
            if self.tree_store.iter_has_child(child):
                children_with_children += 1
            else:
                children_without_children += 1
            child = self.tree_store.iter_next(child)

        self.tree_store[tree_iter][0] += " - {}/{}".format(children_with_children, children_without_children)

    def __on_collapse(self, tree_widget, tree_iter, path):
        if self.tree_store.iter_parent(tree_iter) is not None:
            return

        name = self.tree_store[tree_iter][0]

        i = 0
        for i in range(-1, -len(name), -1):
            if name[i] == '-':
                break
        self.tree_store[tree_iter][0] = name[:i - 1]


def main():
    if len(argv) != 2:
        print("Database path parameter is required!")
        exit(1)
    window = DbViewer()
    window.connect("destroy", Gtk.main_quit)
    window.set_from_db(argv[1])
    window.show_all()
    Gtk.main()


if __name__ == "__main__":
    main()
