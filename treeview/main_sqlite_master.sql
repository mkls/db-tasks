INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'city', 'city', 2, 'CREATE TABLE city
(
	id integer not null
		constraint city_pk
			primary key autoincrement,
	name varchar
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'sqlite_sequence', 'sqlite_sequence', 3, 'CREATE TABLE sqlite_sequence(name,seq)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'city_id_uindex', 'city', 4, 'CREATE UNIQUE INDEX city_id_uindex
	on city (id)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'manufacturer', 'manufacturer', 6, 'CREATE TABLE "manufacturer"
(
	id integer not null
		constraint manufacturer_pk
			primary key autoincrement,
	name varchar,
	city integer
		constraint fk_city_id
			references city
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'manufacturer_id_uindex', 'manufacturer', 5, 'CREATE UNIQUE INDEX manufacturer_id_uindex
	on manufacturer (id)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('table', 'weapon', 'weapon', 7, 'CREATE TABLE weapon
(
	id integer not null
		constraint weapon_pk
			primary key autoincrement,
	name varchar not null,
	manufacturer integer
		constraint weapon_manufacturer_id_fk
			references manufacturer
)');
INSERT INTO sqlite_master (type, name, tbl_name, rootpage, sql) VALUES ('index', 'weapon_id_uindex', 'weapon', 8, 'CREATE UNIQUE INDEX weapon_id_uindex
	on weapon (id)');