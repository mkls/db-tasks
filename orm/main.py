import model
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from os import getenv
from ui import Viewer


def main():
    db_user = getenv('DB_USER')
    db_name = getenv('DB_NAME')
    db_pass = getenv('DB_PASSWORD')

    engine = create_engine(
        'postgresql://{}:{}@localhost/{}'.format(db_user, db_pass, db_name)
    )

    session = sessionmaker(bind=engine)()
    package_managers = session.query(model.PackageManager). \
        order_by(model.PackageManager.id)
    pm_types = session.query(model.PackageManagerType). \
        order_by(model.PackageManagerType.id)

    # For some reason GtkCellRendererCombo creates its own column for
    # model internally and requires that the text column is *exactly*
    # text, no casting allowed. Pity.
    combo_storage = []
    for t in pm_types:
        combo_storage.append((str(t), t))
    combo_storage.append(('<empty>', None))

    app = Viewer(session)
    app.load_data_from_list(app.pm_store, package_managers)
    app.load_data_from_list(app.pm_type_store, pm_types)
    app.load_tuples(app.pm_type_combo, combo_storage)
    app.show_all()

    Gtk.main()

    # session.rollback()


if __name__ == '__main__':
    main()
