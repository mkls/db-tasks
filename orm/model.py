from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class PackageManagerType(Base):
    __tablename__ = 'package_manager_type'
    __table_args__ = {'schema': 'os'}

    id = Column(Integer, primary_key=True)
    name = Column(String)
    description = Column(String)

    fields = {
        'id': 'id',
        'name': 'name',
        'description': 'description'
    }

    package_managers = relationship(
        'PackageManager',
        back_populates='type'
    )

    def get_value(self, field: str):
        return getattr(self, self.fields[field])

    def set_value(self, field: str, value) -> None:
        setattr(self, self.fields[field], value)

    def __str__(self):
        return '{}'.format(self.name)

    def __repr__(self):
        return '<PackageManagerType(id={}, name={}, description={})>'. \
            format(self.id, self.name, self.description)


class PackageManager(Base):
    __tablename__ = 'package_manager'
    __table_args__ = {'schema': 'os'}

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False, default='')
    type_id = Column(
        Integer,
        ForeignKey(
            'os.package_manager_type.id',
            ondelete='SET NULL',
            onupdate='CASCADE'
        )
    )
    pkg_format = Column(String)
    delta_support = Column(Boolean, nullable=False, default=True)

    fields = {
        'pm_id': 'id',
        'pm_name': 'name',
        'pm_type_id': 'type_id',
        'pm_pkg_format': 'pkg_format',
        'pm_delta': 'delta_support',
        'pm_type': 'type'
    }

    type_fields = {
        'type_name': 'name',
        'type_description': 'description'
    }

    type = relationship(
        'PackageManagerType',
        uselist=False,
        back_populates='package_managers'
    )

    def get_value(self, field: str):
        if field in self.type_fields:
            if self.type is None:
                return None
            return self.type.get_value(self.type_fields[field])
        return getattr(self, self.fields[field])

    def set_value(self, field: str, value) -> None:
        if field in self.type_fields:
            self.type.set_value(self.type_fields[field], value)
        else:
            setattr(self, self.fields[field], value)

    def __str__(self):
        return '{}'.format(self.name)

    def __repr__(self):
        return '<PackageManager(id={}, name={}, type_id={}, format={})>' \
               '\ttype: {}'. \
            format(self.id, self.name, self.type_id, self.pkg_format, self.type)


def compare_items(item1, item2, *props):
    for i in props:
        if getattr(item1, i) != getattr(item2, i):
            return False
    return True


def find_dup(source, item, *props):
    for i in source:
        if i[0] is not item and compare_items(i[0], item, *props):
            return True
    return False
