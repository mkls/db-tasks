import gi
import re

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

from model import PackageManager, PackageManagerType, compare_items, find_dup


class Viewer:
    def __init__(self, session):
        self.builder = Gtk.Builder()
        self.builder.add_from_file('ui.glade')

        self.session = session

        self.window = self.builder.get_object('app_window')

        self.dirty_iters = {}

        # Store models
        self.pm_store = self.builder.get_object('pm_store')
        self.pm_type_store = self.builder.get_object('pm_type_store')
        self.pm_type_combo = self.builder.get_object('pm_type_combo')

        # Headerbar buttons
        self.add_row_button = self.builder.get_object('add_row_button')
        self.remove_row_button = self.builder.get_object('remove_row_button')

        # Notebook
        self.notebook = self.builder.get_object('notebook')

        # Notebook operation buttons
        self.commit_button = self.builder.get_object('commit_button')
        self.restore_button = self.builder.get_object('restore_button')

        # Infobar
        self.info_bar = self.builder.get_object('info_bar')
        self.info_bar_label = self.builder.get_object('info_bar_label')

        # Columns
        self.pm_name_column = self.builder.get_object('pm_name_column')
        self.pm_type_column = self.builder.get_object('pm_type_column')
        self.pm_format_column = self.builder.get_object('pm_format_column')
        self.pm_delta_column = self.builder.get_object('pm_delta_column')

        self.type_column = self.builder.get_object('type_column')
        self.description_column = self.builder.get_object('description_column')

        # Cell renderers
        self.pm_name_renderer = self.builder.get_object('pm_name_renderer')
        self.type_name_renderer = self.builder.get_object('type_name_renderer')
        self.pkg_format_renderer = self.builder.get_object('pkg_format_renderer')
        self.pm_delta_renderer = self.builder.get_object('pm_delta_renderer')

        self.pm_type_renderer = self.builder.get_object('pm_type_renderer')
        self.description_renderer = self.builder.get_object('description_renderer')

        # Tree views
        self.pm_view = self.builder.get_object('pm_view')
        self.pm_type_view = self.builder.get_object('pm_type_view')

        # Tree selections
        self.pm_selection = self.builder.get_object('pm_selection')
        self.pm_type_selection = self.builder.get_object('pm_type_selection')

        # Connect to signals, set renderer functions
        self.set_renderers()
        self.connect_handlers()

    def set_renderers(self):
        self.pm_name_column.set_cell_data_func(
            self.pm_name_renderer,
            self.render_text,
            'pm_name'
        )

        self.pm_type_column.set_cell_data_func(
            self.type_name_renderer,
            self.render_text,
            'type_name'
        )

        self.pm_format_column.set_cell_data_func(
            self.pkg_format_renderer,
            self.render_text,
            'pm_pkg_format'
        )

        self.type_column.set_cell_data_func(
            self.pm_type_renderer,
            self.render_text,
            'name'
        )

        self.description_column.set_cell_data_func(
            self.description_renderer,
            self.render_text,
            'description'
        )

        self.pm_delta_column.set_cell_data_func(
            self.pm_delta_renderer,
            self.render_checkbox,
            'pm_delta'
        )

    def connect_handlers(self):
        self.pm_name_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'pm_name'
        )

        self.type_name_renderer.connect(
            'changed',
            self.combo_changed,
            1,
            'pm_type_id',
            'id',
            'pm_type',
            self.pm_store
        )

        self.pkg_format_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'pm_pkg_format'
        )

        self.pm_delta_renderer.connect(
            'toggled',
            self.checkbox_toggled
        )

        self.pm_type_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'name'
        )

        self.description_renderer.connect(
            'edited',
            self.on_text_entry_updated,
            'description'
        )

        self.commit_button.connect(
            'clicked',
            self.commit_clicked
        )

        self.restore_button.connect(
            'clicked',
            self.restore_clicked
        )

        handlers = {
            'on_destroy': Gtk.main_quit,
            'on_text_entry_edited': self.on_text_entry_updated,
            'on_insert_clicked': self.on_insert_clicked,
            'on_selection_changed': self.selection_changed,
            'on_delete_row_clicked': self.delete_clicked,
            'editing_sync': self.editing_sync,
            'editing-canceled': self.editing_canceled
        }

        self.builder.connect_signals(handlers)

    def append_to_storage(self, target, item):
        target.append(item)

    def load_tuples(self, target, tuples):
        for t in tuples:
            target.append(t)

    def load_data_from_list(self, target, list):
        for item in list:
            target.append((item,))

    def load_from_db(self):
        pass

    def show_all(self):
        self.window.show_all()

    def render_checkbox(self, column, cell, model, model_iter, prop):
        obj = model[model_iter][0]
        if obj.get_value('pm_delta') is None:
            obj.set_value('pm_delta', False)

        cell.set_property('active', obj.get_value(prop))
        self.update_cell_color(cell, model, model_iter)

    def render_text(self, column, cell, model, model_iter, prop):
        proxy_item = model[model_iter][0]
        if proxy_item is None:
            text = None
        else:
            text = proxy_item.get_value(prop)
        if text is None:
            text = '<empty>'

        cell.set_property('text', text)
        self.update_cell_color(cell, model, model_iter)

    def get_model_str_repr(self, model):
        if model is self.pm_store:
            return 'pm'
        if model is self.pm_type_store:
            return 'pm_type'
        return None

    def on_text_entry_updated(self, cell, path, new_text, prop):
        trimmed_text = re.sub(' +', ' ', new_text)
        trimmed_text = trimmed_text.strip()

        if self.notebook.get_current_page() == 0:
            current_model = self.pm_store
        else:
            current_model = self.pm_type_store

        prop_type = type(current_model[path][0].get_value(prop))
        if prop_type is type(None):
            current_model[path][0].set_value(prop, trimmed_text)
        else:
            current_model[path][0].set_value(prop, prop_type(trimmed_text))

        m = self.get_model_str_repr(current_model)
        if (m, path) not in self.dirty_iters:
            self.dirty_iters[(m, path)] = 'm'

        self.sync_buttons()

    def combo_changed(self, combo, str_path, new_iter, *args):
        """args contains object column num, property name and updated model"""
        obj = combo.props.model[new_iter][args[0]]
        prop1 = args[1]
        prop2 = args[2]
        prop3 = args[3]
        model = args[4]

        if obj is None:
            model[str_path][0].set_value(prop1, None)
        else:
            model[str_path][0].set_value(prop1, obj.get_value(prop2))
        model[str_path][0].set_value(prop3, obj)

        m = self.get_model_str_repr(model)
        if (m, str_path) not in self.dirty_iters:
            self.dirty_iters[(m, str_path)] = 'm'
        self.sync_buttons()

    def checkbox_toggled(self, cell, path):
        obj = self.pm_store[path][0]
        if obj.get_value('pm_delta') is None:
            val = False
        else:
            val = obj.get_value('pm_delta') ^ True
        obj.set_value('pm_delta', val)

        m = 'pm'
        if (m, path) not in self.dirty_iters:
            self.dirty_iters[(m, path)] = 'm'
        self.sync_buttons()

    def sync_buttons(self, modification=False):
        dirty = len(self.session.dirty) > 0
        new = len(self.session.new) > 0
        deleted = len(self.session.deleted) > 0

        if (dirty or new or deleted) and not modification:
            self.restore_button.set_sensitive(True)
            self.commit_button.set_sensitive(True)
        else:
            self.restore_button.set_sensitive(False)
            self.commit_button.set_sensitive(False)

    def set_info_bar(self, msg: str, show: bool):
        self.info_bar_label.set_text(msg)
        self.info_bar.set_revealed(show)

    def selection_changed(self, selection):
        if selection.get_selected() is None:
            self.sync_buttons(modification=True)
        else:
            self.sync_buttons()


    def on_insert_clicked(self, *args):
        if self.notebook.get_current_page() == 0:
            current_model = self.pm_store
            current_view = self.pm_view
            current_type = PackageManager
        else:
            current_model = self.pm_type_store
            current_view = self.pm_type_view
            current_type = PackageManagerType

        selection = current_view.get_selection()
        new_entry = current_type()
        self.session.add(new_entry)
        new_iter = current_model.append((new_entry,))
        path_str = current_model.get_string_from_iter(new_iter)
        m = self.get_model_str_repr(current_model)
        self.dirty_iters[(m, path_str)] = 'i'
        selection.select_iter(new_iter)

        self.sync_buttons()

    def model_from_str_repr(self, repr: str):
        if repr == 'pm':
            return self.pm_store
        if repr == 'pm_type':
            return self.pm_type_store

    def commit_clicked(self, *args):
        self.set_info_bar('', False)
        iters = []
        for (m, p), s in self.dirty_iters.items():
            if s is None:
                continue
            model = self.model_from_str_repr(m)
            if s == 'd':
                iters.append(
                    (model, model.get_iter_from_string(p))
                )
            else:
                if model is self.pm_store:
                    props = ('name', 'type_id', 'delta_support', 'pkg_format')
                    selection = self.pm_selection
                else:
                    props = ('name', 'description')
                    selection = self.pm_type_selection

                p_iter = model.get_iter_from_string(p)
                item = model[p][0]
                if getattr(item, 'name') is None:
                    self.set_info_bar('Name cannot be empty', True)
                    return
                if find_dup(model, item, *props):
                    selection.select_iter(p_iter)
                    self.set_info_bar('Duplicates are not allowed here', True)
                    return

        for m, i in iters:
            m.remove(i)

        pm_types = self.session.query(PackageManagerType). \
            order_by(PackageManagerType.id)
        combo_storage = []
        for t in pm_types:
            combo_storage.append((str(t), t))
        combo_storage.append(('<empty>', None))

        self.pm_type_combo.clear()
        self.load_tuples(self.pm_type_combo, combo_storage)

        iters.clear()

        self.session.commit()
        self.sync_buttons()
        self.clear_dirty_dict()

    def restore_clicked(self, *args):
        self.set_info_bar('', False)
        iters = []
        for (m, p), s in self.dirty_iters.items():
            if s is None:
                continue
            if s == 'i':
                model = self.model_from_str_repr(m)
                p_iter = model.get_iter_from_string(p)
                iters.append((model, p_iter))

        for model, iter in iters:
            model.remove(iter)

        self.session.rollback()
        self.sync_buttons()
        self.clear_dirty_dict()

    def clear_dirty_dict(self):
        self.dirty_iters.clear()

    def editing_canceled(self):
        self.sync_buttons(modification=False)

    def editing_sync(self, cell, editable, path):
        self.sync_buttons(modification=True)

    def update_cell_color(self, cell, model, cell_iter):
        if model is self.pm_store:
            m = 'pm'
        else:
            m = 'pm_type'
        path_str = model.get_string_from_iter(cell_iter)
        key = (m, path_str)
        if key in self.dirty_iters:
            if self.dirty_iters[key] == 'd':
                cell.props.cell_background_rgba = Gdk.RGBA(0.8, 0, 0, 0.4)
            elif self.dirty_iters[key] == 'm':
                cell.props.cell_background_rgba = Gdk.RGBA(1, 1, 0.5, 0.7)
            elif self.dirty_iters[key] == 'i':
                cell.props.cell_background_rgba = Gdk.RGBA(0, 0.4, 0.1, 0.4)
        else:
            cell.props.cell_background_rgba = None

    def delete_clicked(self, *args):
        if self.notebook.get_current_page() == 0:
            current_selection = self.pm_selection
            m = 'pm'
        else:
            current_selection = self.pm_type_selection
            m = 'pm_type'

        model, selected = current_selection.get_selected()
        path_str = model.get_string_from_iter(selected)
        if (m, path_str) in self.dirty_iters and \
                self.dirty_iters[(m, path_str)] == 'i':
            self.dirty_iters[(m, path_str)] = None
        else:
            self.dirty_iters[(m, path_str)] = 'd'

        obj = model[selected][0]

        if obj not in self.session.new:
            self.session.delete(obj)
        else:
            model.remove(selected)
            self.session.expunge(obj)

        self.set_info_bar('', False)
        self.sync_buttons()
